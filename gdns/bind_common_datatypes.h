/*
 * This file contains basic datatypes for use througout the application.
 */

#ifndef BIND_COMMON_DATATYPES_H
#define BIND_COMMON_DATATYPES_H

/* The yes_no type */

typedef enum bind_bool_tag
{
  bind_bool_yes = 1,
  bind_bool_no,
} bind_bool_t;

/* types of fowrwarding allowed */

typedef enum bind_forward_tag
{
  bind_forward_only = 1,
  bind_forward_first,
} bind_forward_t;

/* types of name checking */

typedef enum bind_name_check_tag
{
  bind_name_check_ignore = 1,
  bind_name_check_warn,
  bind_name_check_fail,
} bind_name_check_t;

/* just for now */

/* types of address types.  One is for address matching,
   the other a simple address. */

typedef void * bind_addr_match_list_t;
typedef void * bind_addr_t;

/* This is for an address match and port pair */

typedef struct bind_addr_match_port_pair_tag
{
  int port;
  bind_addr_match_list_t *match_list;
} bind_addr_match_port_pair_t;

/* these are the types of transfers available */

typedef enum bind_transfer_type_tag
{
  bind_transfer_type_one_answer = 1,
  bind_transfer_type_many_answer,
} bind_transfer_type_t;

/* this is a "size spec".  It's either "unlimited" or a positive number */

typedef char * bind_size_spec_t;

/* this is an include file */

typedef char * bind_include_t;

#endif /* BIND_COMMON_DATATYPES_H */
