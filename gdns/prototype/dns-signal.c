/*  Note: You are free to use whatever license you want.
    Eventually you will be able to edit it within Glade. */

/*  gdns
 *  Copyright (C) <YEAR> <AUTHORS>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <gtk/gtkintl.h>
#include "gdns.h"
#include "dns-signal.h"

int
main (int argc, char *argv[])
{
  GtkWidget *win_zone_list;
  GtkWidget *win_zone_editor;
  GtkWidget *dlg_new_zone_type;
  GtkWidget *dlg_new_hint_zone;
  GtkWidget *dlg_new_slave_zone;
  GtkWidget *dlg_new_master_zone;
  GtkWidget *dlg_server_options;

  gtk_set_locale ();
  gtk_init (&argc, &argv);

  /*
   * The following code was added by Glade to create one of each component
   * (except popup menus), just so that you see something after building
   * the project. Delete any components that you don't want shown initially.
   */
  win_zone_list = create_win_zone_list ();
  gtk_widget_show (win_zone_list);
  win_zone_editor = create_win_zone_editor ();
  gtk_widget_show (win_zone_editor);
  dlg_new_zone_type = create_dlg_new_zone_type ();
  gtk_widget_show (dlg_new_zone_type);
  dlg_new_hint_zone = create_dlg_new_hint_zone ();
  gtk_widget_show (dlg_new_hint_zone);
  dlg_new_slave_zone = create_dlg_new_slave_zone ();
  gtk_widget_show (dlg_new_slave_zone);
  dlg_new_master_zone = create_dlg_new_master_zone ();
  gtk_widget_show (dlg_new_master_zone);
  dlg_server_options = create_dlg_server_options ();
  gtk_widget_show (dlg_server_options);

  gtk_main ();
  return 0;
}


void
menu_file_new                          (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_file_open                         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_file_save                         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_file_save_as                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_file_exit                         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_edit_cut                          (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_edit_copy                         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_edit_paste                        (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_edit_delete                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_edit_svr_opt_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_zone_new                          (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_zone_edit                         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_help_about                        (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_zone_file_save                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_zone_file_close                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_zone_edit_cut                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_zone_edit_copy                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_zone_edit_paste                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_zone_edit_delete                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_zone_record_new                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
menu_zone_record_edit                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
zn_type_master_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
zn_type_slave_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
zn_type_stub_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
zn_type_hint_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
zn_type_cancel_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
zn_type_help_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
hnt_zn_browse_hint_file_clicked        (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
hnt_zn_ok_clicked                      (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
hnt_zn_cancel_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
hnt_zn_help_clicked                    (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
slv_zn_add_srvr_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
slv_zn_del_srvr_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
slv_zn_acl_update_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
slv_zn_acl_query_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
slv_zn_acl_transfer_clicked            (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
slv_zn_also_notify_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
slv_zn_ok_clicked                      (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
slv_zn_cancel_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
slv_zn_help_clicked                    (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
mst_zn_edit_rr_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
mst_zn_acl_update_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
mst_zn_acl_query_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
mst_zn_acl_transfer_clicked            (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
mst_zn_also_notify_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
mst_zn_ok_clicked                      (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
mst_zn_cancel_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
mst_zn_help_clicked                    (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_dlg_server_options_destroy          (GtkObject       *object,
                                        gpointer         user_data)
{

}


gboolean
on_dlg_server_options_delete_event     (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
svr_opt_fwd_add_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
svr_opt_fwd_del_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
svr_opt_ok_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
svr_opt_cancel_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
svr_opt_help_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{

}

