/*  Note: You are free to use whatever license you want.
    Eventually you will be able to edit it within Glade. */

/*  gdns
 *  Copyright (C) <YEAR> <AUTHORS>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <gtk/gtkintl.h>
#include <gdk/gdkkeysyms.h>
#include "dns-signal.h"
#include "gdns.h"

GtkWidget*
get_widget                             (GtkWidget       *widget,
                                        gchar           *widget_name)
{
  GtkWidget *parent, *found_widget;

  for (;;)
    {
      if (GTK_IS_MENU (widget))
        parent = gtk_menu_get_attach_widget (GTK_MENU (widget));
      else
        parent = widget->parent;
      if (parent == NULL)
        break;
      widget = parent;
    }

  found_widget = (GtkWidget*) gtk_object_get_data (GTK_OBJECT (widget),
                                                   widget_name);
  if (!found_widget)
    g_warning ("Widget not found: %s", widget_name);
  return found_widget;
}

/* This is an internally used function to set notebook tab widgets. */
void
set_notebook_tab                       (GtkWidget       *notebook,
                                        gint             page_num,
                                        GtkWidget       *widget)
{
  GtkNotebookPage *page;
  GtkWidget *notebook_page;

  page = (GtkNotebookPage*) g_list_nth (GTK_NOTEBOOK (notebook)->children, page_num)->data;
  notebook_page = page->child;
  gtk_widget_ref (notebook_page);
  gtk_notebook_remove_page (GTK_NOTEBOOK (notebook), page_num);
  gtk_notebook_insert_page (GTK_NOTEBOOK (notebook), notebook_page,
                            widget, page_num);
  gtk_widget_unref (notebook_page);
}

GtkWidget*
create_win_zone_list ()
{
  GtkWidget *win_zone_list;
  GtkWidget *vbox1;
  GtkWidget *mb_zone_list;
  GtkWidget *File;
  GtkWidget *menu33;
  GtkWidget *New;
  GtkWidget *Open;
  GtkWidget *separator1;
  GtkWidget *Save;
  GtkWidget *Save_As;
  GtkWidget *separator2;
  GtkWidget *Exit;
  GtkWidget *Edit;
  GtkWidget *menu34;
  GtkWidget *Cut;
  GtkWidget *Copy;
  GtkWidget *Paste;
  GtkWidget *separator3;
  GtkWidget *Delete;
  GtkWidget *separator4;
  GtkWidget *Server_Options;
  GtkWidget *Zone;
  GtkWidget *menu35;
  GtkWidget *NewZone;
  GtkWidget *EditZone;
  GtkWidget *Help;
  GtkWidget *menu36;
  GtkWidget *About;
  GtkWidget *cl_zones;
  GtkWidget *lbl_zone;
  GtkWidget *lbl_type;
  GtkWidget *lbl_extra;
  GtkWidget *sb_zone_list;
  GtkAccelGroup *accel_group;

  win_zone_list = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "win_zone_list", win_zone_list);
  gtk_window_set_title (GTK_WINDOW (win_zone_list), _("gdns - Zone List"));
  gtk_window_set_policy (GTK_WINDOW (win_zone_list), TRUE, TRUE, FALSE);

  vbox1 = gtk_vbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "vbox1", vbox1);
  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (win_zone_list), vbox1);

  mb_zone_list = gtk_menu_bar_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "mb_zone_list", mb_zone_list);
  gtk_widget_show (mb_zone_list);
  gtk_box_pack_start (GTK_BOX (vbox1), mb_zone_list, FALSE, TRUE, 0);

  File = gtk_menu_item_new_with_label (_("File"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "File", File);
  gtk_widget_show (File);
  gtk_container_add (GTK_CONTAINER (mb_zone_list), File);

  menu33 = gtk_menu_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "menu33", menu33);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (File), menu33);

  New = gtk_menu_item_new_with_label (_("New"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "New", New);
  gtk_widget_show (New);
  gtk_container_add (GTK_CONTAINER (menu33), New);
  gtk_signal_connect (GTK_OBJECT (New), "activate",
                      GTK_SIGNAL_FUNC (menu_file_new),
                      NULL);
  accel_group = gtk_accel_group_new ();
  gtk_window_add_accel_group (GTK_WINDOW (win_zone_list), accel_group);
  gtk_widget_add_accelerator (New, "activate", accel_group,
                              GDK_N, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  Open = gtk_menu_item_new_with_label (_("Open..."));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "Open", Open);
  gtk_widget_show (Open);
  gtk_container_add (GTK_CONTAINER (menu33), Open);
  gtk_signal_connect (GTK_OBJECT (Open), "activate",
                      GTK_SIGNAL_FUNC (menu_file_open),
                      NULL);
  gtk_widget_add_accelerator (Open, "activate", accel_group,
                              GDK_O, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  separator1 = gtk_menu_item_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "separator1", separator1);
  gtk_widget_show (separator1);
  gtk_container_add (GTK_CONTAINER (menu33), separator1);

  Save = gtk_menu_item_new_with_label (_("Save"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "Save", Save);
  gtk_widget_show (Save);
  gtk_container_add (GTK_CONTAINER (menu33), Save);
  gtk_signal_connect (GTK_OBJECT (Save), "activate",
                      GTK_SIGNAL_FUNC (menu_file_save),
                      NULL);
  gtk_widget_add_accelerator (Save, "activate", accel_group,
                              GDK_S, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  Save_As = gtk_menu_item_new_with_label (_("Save As..."));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "Save_As", Save_As);
  gtk_widget_show (Save_As);
  gtk_container_add (GTK_CONTAINER (menu33), Save_As);
  gtk_signal_connect (GTK_OBJECT (Save_As), "activate",
                      GTK_SIGNAL_FUNC (menu_file_save_as),
                      NULL);

  separator2 = gtk_menu_item_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "separator2", separator2);
  gtk_widget_show (separator2);
  gtk_container_add (GTK_CONTAINER (menu33), separator2);

  Exit = gtk_menu_item_new_with_label (_("Exit"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "Exit", Exit);
  gtk_widget_show (Exit);
  gtk_container_add (GTK_CONTAINER (menu33), Exit);
  gtk_signal_connect (GTK_OBJECT (Exit), "activate",
                      GTK_SIGNAL_FUNC (menu_file_exit),
                      NULL);
  gtk_widget_add_accelerator (Exit, "activate", accel_group,
                              GDK_X, GDK_MOD1_MASK, GTK_ACCEL_VISIBLE);

  Edit = gtk_menu_item_new_with_label (_("Edit"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "Edit", Edit);
  gtk_widget_show (Edit);
  gtk_container_add (GTK_CONTAINER (mb_zone_list), Edit);

  menu34 = gtk_menu_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "menu34", menu34);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (Edit), menu34);

  Cut = gtk_menu_item_new_with_label (_("Cut"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "Cut", Cut);
  gtk_widget_show (Cut);
  gtk_container_add (GTK_CONTAINER (menu34), Cut);
  gtk_signal_connect (GTK_OBJECT (Cut), "activate",
                      GTK_SIGNAL_FUNC (menu_edit_cut),
                      NULL);
  gtk_widget_add_accelerator (Cut, "activate", accel_group,
                              GDK_X, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  Copy = gtk_menu_item_new_with_label (_("Copy"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "Copy", Copy);
  gtk_widget_show (Copy);
  gtk_container_add (GTK_CONTAINER (menu34), Copy);
  gtk_signal_connect (GTK_OBJECT (Copy), "activate",
                      GTK_SIGNAL_FUNC (menu_edit_copy),
                      NULL);
  gtk_widget_add_accelerator (Copy, "activate", accel_group,
                              GDK_C, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  Paste = gtk_menu_item_new_with_label (_("Paste"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "Paste", Paste);
  gtk_widget_show (Paste);
  gtk_container_add (GTK_CONTAINER (menu34), Paste);
  gtk_signal_connect (GTK_OBJECT (Paste), "activate",
                      GTK_SIGNAL_FUNC (menu_edit_paste),
                      NULL);
  gtk_widget_add_accelerator (Paste, "activate", accel_group,
                              GDK_V, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  separator3 = gtk_menu_item_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "separator3", separator3);
  gtk_widget_show (separator3);
  gtk_container_add (GTK_CONTAINER (menu34), separator3);

  Delete = gtk_menu_item_new_with_label (_("Delete"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "Delete", Delete);
  gtk_widget_show (Delete);
  gtk_container_add (GTK_CONTAINER (menu34), Delete);
  gtk_signal_connect (GTK_OBJECT (Delete), "activate",
                      GTK_SIGNAL_FUNC (menu_edit_delete),
                      NULL);
  gtk_widget_add_accelerator (Delete, "activate", accel_group,
                              GDK_Delete, 0, GTK_ACCEL_VISIBLE);

  separator4 = gtk_menu_item_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "separator4", separator4);
  gtk_widget_show (separator4);
  gtk_container_add (GTK_CONTAINER (menu34), separator4);

  Server_Options = gtk_menu_item_new_with_label (_("Server Options..."));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "Server_Options", Server_Options);
  gtk_widget_show (Server_Options);
  gtk_container_add (GTK_CONTAINER (menu34), Server_Options);
  gtk_signal_connect (GTK_OBJECT (Server_Options), "activate",
                      GTK_SIGNAL_FUNC (menu_edit_svr_opt_activate),
                      NULL);
  gtk_widget_add_accelerator (Server_Options, "activate", accel_group,
                              GDK_O, GDK_MOD1_MASK, GTK_ACCEL_VISIBLE);

  Zone = gtk_menu_item_new_with_label (_("Zone"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "Zone", Zone);
  gtk_widget_show (Zone);
  gtk_container_add (GTK_CONTAINER (mb_zone_list), Zone);

  menu35 = gtk_menu_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "menu35", menu35);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (Zone), menu35);

  NewZone = gtk_menu_item_new_with_label (_("New..."));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "NewZone", NewZone);
  gtk_widget_show (NewZone);
  gtk_container_add (GTK_CONTAINER (menu35), NewZone);
  gtk_signal_connect (GTK_OBJECT (NewZone), "activate",
                      GTK_SIGNAL_FUNC (menu_zone_new),
                      NULL);
  gtk_widget_add_accelerator (NewZone, "activate", accel_group,
                              GDK_Insert, 0, GTK_ACCEL_VISIBLE);

  EditZone = gtk_menu_item_new_with_label (_("Edit..."));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "EditZone", EditZone);
  gtk_widget_show (EditZone);
  gtk_container_add (GTK_CONTAINER (menu35), EditZone);
  gtk_signal_connect (GTK_OBJECT (EditZone), "activate",
                      GTK_SIGNAL_FUNC (menu_zone_edit),
                      NULL);
  gtk_widget_add_accelerator (EditZone, "activate", accel_group,
                              GDK_Return, 0, GTK_ACCEL_VISIBLE);

  Help = gtk_menu_item_new_with_label (_("Help"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "Help", Help);
  gtk_widget_show (Help);
  gtk_container_add (GTK_CONTAINER (mb_zone_list), Help);

  menu36 = gtk_menu_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "menu36", menu36);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (Help), menu36);

  About = gtk_menu_item_new_with_label (_("About..."));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "About", About);
  gtk_widget_show (About);
  gtk_container_add (GTK_CONTAINER (menu36), About);
  gtk_signal_connect (GTK_OBJECT (About), "activate",
                      GTK_SIGNAL_FUNC (menu_help_about),
                      NULL);

  cl_zones = gtk_clist_new (3);
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "cl_zones", cl_zones);
  gtk_widget_show (cl_zones);
  gtk_box_pack_start (GTK_BOX (vbox1), cl_zones, TRUE, TRUE, 0);
  gtk_widget_set_usize (cl_zones, 500, 300);
  gtk_clist_set_column_width (GTK_CLIST (cl_zones), 0, 133);
  gtk_clist_set_column_width (GTK_CLIST (cl_zones), 1, 83);
  gtk_clist_set_column_width (GTK_CLIST (cl_zones), 2, 80);
  gtk_clist_column_titles_show (GTK_CLIST (cl_zones));

  lbl_zone = gtk_label_new (_("Zone"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "lbl_zone", lbl_zone);
  gtk_widget_show (lbl_zone);
  gtk_clist_set_column_widget (GTK_CLIST (cl_zones), 0, lbl_zone);

  lbl_type = gtk_label_new (_("Type"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "lbl_type", lbl_type);
  gtk_widget_show (lbl_type);
  gtk_clist_set_column_widget (GTK_CLIST (cl_zones), 1, lbl_type);

  lbl_extra = gtk_label_new (_("Servers / Source filename"));
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "lbl_extra", lbl_extra);
  gtk_widget_show (lbl_extra);
  gtk_clist_set_column_widget (GTK_CLIST (cl_zones), 2, lbl_extra);

  sb_zone_list = gtk_statusbar_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_list), "sb_zone_list", sb_zone_list);
  gtk_widget_show (sb_zone_list);
  gtk_box_pack_start (GTK_BOX (vbox1), sb_zone_list, TRUE, TRUE, 0);

  return win_zone_list;
}

GtkWidget*
create_win_zone_editor ()
{
  GtkWidget *win_zone_editor;
  GtkWidget *vbox2;
  GtkWidget *mb_zone_editor;
  GtkWidget *File2;
  GtkWidget *menu18;
  GtkWidget *Save2;
  GtkWidget *separator4;
  GtkWidget *Close;
  GtkWidget *Edit3;
  GtkWidget *menu19;
  GtkWidget *Cut2;
  GtkWidget *Copy2;
  GtkWidget *Paste2;
  GtkWidget *separator5;
  GtkWidget *Delete2;
  GtkWidget *Record;
  GtkWidget *menu20;
  GtkWidget *New2;
  GtkWidget *Edit2;
  GtkWidget *cl_rr_list;
  GtkWidget *lbl_rr_name;
  GtkWidget *lbl_rr_type;
  GtkWidget *lbl_rr_value;
  GtkWidget *sb_zone_editor;
  GtkAccelGroup *accel_group;

  win_zone_editor = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "win_zone_editor", win_zone_editor);
  gtk_window_set_title (GTK_WINDOW (win_zone_editor), _("Zone Editor"));
  gtk_window_set_policy (GTK_WINDOW (win_zone_editor), TRUE, TRUE, FALSE);

  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "vbox2", vbox2);
  gtk_widget_show (vbox2);
  gtk_container_add (GTK_CONTAINER (win_zone_editor), vbox2);

  mb_zone_editor = gtk_menu_bar_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "mb_zone_editor", mb_zone_editor);
  gtk_widget_show (mb_zone_editor);
  gtk_box_pack_start (GTK_BOX (vbox2), mb_zone_editor, FALSE, TRUE, 0);

  File2 = gtk_menu_item_new_with_label (_("File"));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "File2", File2);
  gtk_widget_show (File2);
  gtk_container_add (GTK_CONTAINER (mb_zone_editor), File2);

  menu18 = gtk_menu_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "menu18", menu18);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (File2), menu18);

  Save2 = gtk_menu_item_new_with_label (_("Save"));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "Save2", Save2);
  gtk_widget_show (Save2);
  gtk_container_add (GTK_CONTAINER (menu18), Save2);
  gtk_signal_connect (GTK_OBJECT (Save2), "activate",
                      GTK_SIGNAL_FUNC (menu_zone_file_save),
                      NULL);
  accel_group = gtk_accel_group_new ();
  gtk_window_add_accel_group (GTK_WINDOW (win_zone_editor), accel_group);
  gtk_widget_add_accelerator (Save2, "activate", accel_group,
                              GDK_S, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  separator4 = gtk_menu_item_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "separator4", separator4);
  gtk_widget_show (separator4);
  gtk_container_add (GTK_CONTAINER (menu18), separator4);

  Close = gtk_menu_item_new_with_label (_("Close"));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "Close", Close);
  gtk_widget_show (Close);
  gtk_container_add (GTK_CONTAINER (menu18), Close);
  gtk_signal_connect (GTK_OBJECT (Close), "activate",
                      GTK_SIGNAL_FUNC (menu_zone_file_close),
                      NULL);
  gtk_widget_add_accelerator (Close, "activate", accel_group,
                              GDK_W, GDK_MOD1_MASK, GTK_ACCEL_VISIBLE);

  Edit3 = gtk_menu_item_new_with_label (_("Edit"));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "Edit3", Edit3);
  gtk_widget_show (Edit3);
  gtk_container_add (GTK_CONTAINER (mb_zone_editor), Edit3);

  menu19 = gtk_menu_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "menu19", menu19);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (Edit3), menu19);

  Cut2 = gtk_menu_item_new_with_label (_("Cut"));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "Cut2", Cut2);
  gtk_widget_show (Cut2);
  gtk_container_add (GTK_CONTAINER (menu19), Cut2);
  gtk_signal_connect (GTK_OBJECT (Cut2), "activate",
                      GTK_SIGNAL_FUNC (menu_zone_edit_cut),
                      NULL);
  gtk_widget_add_accelerator (Cut2, "activate", accel_group,
                              GDK_C, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  Copy2 = gtk_menu_item_new_with_label (_("Copy"));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "Copy2", Copy2);
  gtk_widget_show (Copy2);
  gtk_container_add (GTK_CONTAINER (menu19), Copy2);
  gtk_signal_connect (GTK_OBJECT (Copy2), "activate",
                      GTK_SIGNAL_FUNC (menu_zone_edit_copy),
                      NULL);
  gtk_widget_add_accelerator (Copy2, "activate", accel_group,
                              GDK_X, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  Paste2 = gtk_menu_item_new_with_label (_("Paste"));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "Paste2", Paste2);
  gtk_widget_show (Paste2);
  gtk_container_add (GTK_CONTAINER (menu19), Paste2);
  gtk_signal_connect (GTK_OBJECT (Paste2), "activate",
                      GTK_SIGNAL_FUNC (menu_zone_edit_paste),
                      NULL);
  gtk_widget_add_accelerator (Paste2, "activate", accel_group,
                              GDK_V, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  separator5 = gtk_menu_item_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "separator5", separator5);
  gtk_widget_show (separator5);
  gtk_container_add (GTK_CONTAINER (menu19), separator5);

  Delete2 = gtk_menu_item_new_with_label (_("Delete"));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "Delete2", Delete2);
  gtk_widget_show (Delete2);
  gtk_container_add (GTK_CONTAINER (menu19), Delete2);
  gtk_signal_connect (GTK_OBJECT (Delete2), "activate",
                      GTK_SIGNAL_FUNC (menu_zone_edit_delete),
                      NULL);
  gtk_widget_add_accelerator (Delete2, "activate", accel_group,
                              GDK_Delete, 0, GTK_ACCEL_VISIBLE);

  Record = gtk_menu_item_new_with_label (_("Record"));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "Record", Record);
  gtk_widget_show (Record);
  gtk_container_add (GTK_CONTAINER (mb_zone_editor), Record);

  menu20 = gtk_menu_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "menu20", menu20);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (Record), menu20);

  New2 = gtk_menu_item_new_with_label (_("New..."));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "New2", New2);
  gtk_widget_show (New2);
  gtk_container_add (GTK_CONTAINER (menu20), New2);
  gtk_signal_connect (GTK_OBJECT (New2), "activate",
                      GTK_SIGNAL_FUNC (menu_zone_record_new),
                      NULL);
  gtk_widget_add_accelerator (New2, "activate", accel_group,
                              GDK_Insert, 0, GTK_ACCEL_VISIBLE);

  Edit2 = gtk_menu_item_new_with_label (_("Edit..."));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "Edit2", Edit2);
  gtk_widget_show (Edit2);
  gtk_container_add (GTK_CONTAINER (menu20), Edit2);
  gtk_signal_connect (GTK_OBJECT (Edit2), "activate",
                      GTK_SIGNAL_FUNC (menu_zone_record_edit),
                      NULL);
  gtk_widget_add_accelerator (Edit2, "activate", accel_group,
                              GDK_Return, 0, GTK_ACCEL_VISIBLE);

  cl_rr_list = gtk_clist_new (3);
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "cl_rr_list", cl_rr_list);
  gtk_widget_show (cl_rr_list);
  gtk_box_pack_start (GTK_BOX (vbox2), cl_rr_list, TRUE, TRUE, 0);
  gtk_widget_set_usize (cl_rr_list, 300, 300);
  gtk_clist_set_column_width (GTK_CLIST (cl_rr_list), 0, 97);
  gtk_clist_set_column_width (GTK_CLIST (cl_rr_list), 1, 52);
  gtk_clist_set_column_width (GTK_CLIST (cl_rr_list), 2, 80);
  gtk_clist_column_titles_show (GTK_CLIST (cl_rr_list));

  lbl_rr_name = gtk_label_new (_("Name"));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "lbl_rr_name", lbl_rr_name);
  gtk_widget_show (lbl_rr_name);
  gtk_clist_set_column_widget (GTK_CLIST (cl_rr_list), 0, lbl_rr_name);

  lbl_rr_type = gtk_label_new (_("Type"));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "lbl_rr_type", lbl_rr_type);
  gtk_widget_show (lbl_rr_type);
  gtk_clist_set_column_widget (GTK_CLIST (cl_rr_list), 1, lbl_rr_type);

  lbl_rr_value = gtk_label_new (_("Value"));
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "lbl_rr_value", lbl_rr_value);
  gtk_widget_show (lbl_rr_value);
  gtk_clist_set_column_widget (GTK_CLIST (cl_rr_list), 2, lbl_rr_value);

  sb_zone_editor = gtk_statusbar_new ();
  gtk_object_set_data (GTK_OBJECT (win_zone_editor), "sb_zone_editor", sb_zone_editor);
  gtk_widget_show (sb_zone_editor);
  gtk_box_pack_start (GTK_BOX (vbox2), sb_zone_editor, TRUE, TRUE, 0);

  return win_zone_editor;
}

GtkWidget*
create_dlg_new_zone_type ()
{
  GtkWidget *dlg_new_zone_type;
  GtkWidget *dialog_vbox1;
  GtkWidget *vbuttonbox3;
  GtkWidget *btn_master;
  GtkWidget *btn_slave;
  GtkWidget *btn_stub;
  GtkWidget *btn_hint;
  GtkWidget *dialog_action_area1;
  GtkWidget *hbuttonbox1;
  GtkWidget *btn_cancel;
  GtkWidget *btn_help;

  dlg_new_zone_type = gtk_dialog_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_zone_type), "dlg_new_zone_type", dlg_new_zone_type);
  gtk_window_set_title (GTK_WINDOW (dlg_new_zone_type), _("Zone Type"));
  gtk_window_set_policy (GTK_WINDOW (dlg_new_zone_type), TRUE, TRUE, FALSE);

  dialog_vbox1 = GTK_DIALOG (dlg_new_zone_type)->vbox;
  gtk_object_set_data (GTK_OBJECT (dlg_new_zone_type), "dialog_vbox1", dialog_vbox1);
  gtk_widget_show (dialog_vbox1);

  vbuttonbox3 = gtk_vbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_zone_type), "vbuttonbox3", vbuttonbox3);
  gtk_widget_show (vbuttonbox3);
  gtk_box_pack_start (GTK_BOX (dialog_vbox1), vbuttonbox3, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (vbuttonbox3), 5);

  btn_master = gtk_button_new_with_label (_("Master"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_zone_type), "btn_master", btn_master);
  gtk_widget_show (btn_master);
  gtk_container_add (GTK_CONTAINER (vbuttonbox3), btn_master);
  GTK_WIDGET_SET_FLAGS (btn_master, GTK_CAN_DEFAULT);
  gtk_widget_grab_focus (btn_master);
  gtk_widget_grab_default (btn_master);
  gtk_signal_connect (GTK_OBJECT (btn_master), "clicked",
                      GTK_SIGNAL_FUNC (zn_type_master_clicked),
                      NULL);

  btn_slave = gtk_button_new_with_label (_("Slave"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_zone_type), "btn_slave", btn_slave);
  gtk_widget_show (btn_slave);
  gtk_container_add (GTK_CONTAINER (vbuttonbox3), btn_slave);
  gtk_signal_connect (GTK_OBJECT (btn_slave), "clicked",
                      GTK_SIGNAL_FUNC (zn_type_slave_clicked),
                      NULL);

  btn_stub = gtk_button_new_with_label (_("Stub"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_zone_type), "btn_stub", btn_stub);
  gtk_widget_show (btn_stub);
  gtk_container_add (GTK_CONTAINER (vbuttonbox3), btn_stub);
  gtk_signal_connect (GTK_OBJECT (btn_stub), "clicked",
                      GTK_SIGNAL_FUNC (zn_type_stub_clicked),
                      NULL);

  btn_hint = gtk_button_new_with_label (_("Hint"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_zone_type), "btn_hint", btn_hint);
  gtk_widget_show (btn_hint);
  gtk_container_add (GTK_CONTAINER (vbuttonbox3), btn_hint);
  gtk_signal_connect (GTK_OBJECT (btn_hint), "clicked",
                      GTK_SIGNAL_FUNC (zn_type_hint_clicked),
                      NULL);

  dialog_action_area1 = GTK_DIALOG (dlg_new_zone_type)->action_area;
  gtk_object_set_data (GTK_OBJECT (dlg_new_zone_type), "dialog_action_area1", dialog_action_area1);
  gtk_widget_show (dialog_action_area1);
  gtk_container_border_width (GTK_CONTAINER (dialog_action_area1), 10);

  hbuttonbox1 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_zone_type), "hbuttonbox1", hbuttonbox1);
  gtk_widget_show (hbuttonbox1);
  gtk_box_pack_start (GTK_BOX (dialog_action_area1), hbuttonbox1, TRUE, TRUE, 0);

  btn_cancel = gtk_button_new_with_label (_("Cancel"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_zone_type), "btn_cancel", btn_cancel);
  gtk_widget_show (btn_cancel);
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), btn_cancel);
  gtk_signal_connect (GTK_OBJECT (btn_cancel), "clicked",
                      GTK_SIGNAL_FUNC (zn_type_cancel_clicked),
                      NULL);

  btn_help = gtk_button_new_with_label (_("Help"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_zone_type), "btn_help", btn_help);
  gtk_widget_show (btn_help);
  gtk_container_add (GTK_CONTAINER (hbuttonbox1), btn_help);
  gtk_signal_connect (GTK_OBJECT (btn_help), "clicked",
                      GTK_SIGNAL_FUNC (zn_type_help_clicked),
                      NULL);

  return dlg_new_zone_type;
}

GtkWidget*
create_dlg_new_hint_zone ()
{
  GtkWidget *dlg_new_hint_zone;
  GtkWidget *dialog_vbox2;
  GtkWidget *vbox4;
  GtkWidget *hbox2;
  GtkWidget *lbl_hint_file;
  GtkWidget *hbox3;
  GtkWidget *en_hint_file;
  GtkWidget *btn_browse_hint_file;
  GtkWidget *frame1;
  GtkWidget *hbuttonbox3;
  GSList *hbuttonbox3_group = NULL;
  GtkWidget *rb_ck_nm_warn;
  GtkWidget *rb_ck_nm_fail;
  GtkWidget *rb_ck_nm_ignore;
  GtkWidget *dialog_action_area2;
  GtkWidget *hbuttonbox2;
  GtkWidget *btn_ok;
  GtkWidget *btn_cancel;
  GtkWidget *btn_help;

  dlg_new_hint_zone = gtk_dialog_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "dlg_new_hint_zone", dlg_new_hint_zone);
  gtk_window_set_title (GTK_WINDOW (dlg_new_hint_zone), _("New/Edit Hint Zone"));
  gtk_window_set_policy (GTK_WINDOW (dlg_new_hint_zone), TRUE, TRUE, FALSE);

  dialog_vbox2 = GTK_DIALOG (dlg_new_hint_zone)->vbox;
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "dialog_vbox2", dialog_vbox2);
  gtk_widget_show (dialog_vbox2);

  vbox4 = gtk_vbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "vbox4", vbox4);
  gtk_widget_show (vbox4);
  gtk_box_pack_start (GTK_BOX (dialog_vbox2), vbox4, TRUE, TRUE, 0);

  hbox2 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "hbox2", hbox2);
  gtk_widget_show (hbox2);
  gtk_box_pack_start (GTK_BOX (vbox4), hbox2, TRUE, TRUE, 0);

  lbl_hint_file = gtk_label_new (_("File"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "lbl_hint_file", lbl_hint_file);
  gtk_widget_show (lbl_hint_file);
  gtk_box_pack_start (GTK_BOX (hbox2), lbl_hint_file, TRUE, TRUE, 0);

  hbox3 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "hbox3", hbox3);
  gtk_widget_show (hbox3);
  gtk_box_pack_start (GTK_BOX (hbox2), hbox3, TRUE, TRUE, 0);

  en_hint_file = gtk_entry_new_with_max_length (255);
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "en_hint_file", en_hint_file);
  gtk_widget_show (en_hint_file);
  gtk_box_pack_start (GTK_BOX (hbox3), en_hint_file, TRUE, TRUE, 0);
  gtk_widget_grab_focus (en_hint_file);

  btn_browse_hint_file = gtk_button_new_with_label (_(".."));
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "btn_browse_hint_file", btn_browse_hint_file);
  gtk_widget_show (btn_browse_hint_file);
  gtk_box_pack_start (GTK_BOX (hbox3), btn_browse_hint_file, TRUE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (btn_browse_hint_file), "clicked",
                      GTK_SIGNAL_FUNC (hnt_zn_browse_hint_file_clicked),
                      NULL);

  frame1 = gtk_frame_new (_("Check Names"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "frame1", frame1);
  gtk_widget_show (frame1);
  gtk_box_pack_start (GTK_BOX (vbox4), frame1, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (frame1), 3);

  hbuttonbox3 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "hbuttonbox3", hbuttonbox3);
  gtk_widget_show (hbuttonbox3);
  gtk_container_add (GTK_CONTAINER (frame1), hbuttonbox3);

  rb_ck_nm_warn = gtk_radio_button_new_with_label (hbuttonbox3_group, _("Warn"));
  hbuttonbox3_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_ck_nm_warn));
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "rb_ck_nm_warn", rb_ck_nm_warn);
  gtk_widget_show (rb_ck_nm_warn);
  gtk_container_add (GTK_CONTAINER (hbuttonbox3), rb_ck_nm_warn);

  rb_ck_nm_fail = gtk_radio_button_new_with_label (hbuttonbox3_group, _("Fail"));
  hbuttonbox3_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_ck_nm_fail));
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "rb_ck_nm_fail", rb_ck_nm_fail);
  gtk_widget_show (rb_ck_nm_fail);
  gtk_container_add (GTK_CONTAINER (hbuttonbox3), rb_ck_nm_fail);

  rb_ck_nm_ignore = gtk_radio_button_new_with_label (hbuttonbox3_group, _("Ignore"));
  hbuttonbox3_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_ck_nm_ignore));
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "rb_ck_nm_ignore", rb_ck_nm_ignore);
  gtk_widget_show (rb_ck_nm_ignore);
  gtk_container_add (GTK_CONTAINER (hbuttonbox3), rb_ck_nm_ignore);

  dialog_action_area2 = GTK_DIALOG (dlg_new_hint_zone)->action_area;
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "dialog_action_area2", dialog_action_area2);
  gtk_widget_show (dialog_action_area2);
  gtk_container_border_width (GTK_CONTAINER (dialog_action_area2), 10);

  hbuttonbox2 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "hbuttonbox2", hbuttonbox2);
  gtk_widget_show (hbuttonbox2);
  gtk_box_pack_start (GTK_BOX (dialog_action_area2), hbuttonbox2, TRUE, TRUE, 0);

  btn_ok = gtk_button_new_with_label (_("Ok"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "btn_ok", btn_ok);
  gtk_widget_show (btn_ok);
  gtk_container_add (GTK_CONTAINER (hbuttonbox2), btn_ok);
  GTK_WIDGET_SET_FLAGS (btn_ok, GTK_CAN_DEFAULT);
  gtk_widget_grab_default (btn_ok);
  gtk_signal_connect (GTK_OBJECT (btn_ok), "clicked",
                      GTK_SIGNAL_FUNC (hnt_zn_ok_clicked),
                      NULL);

  btn_cancel = gtk_button_new_with_label (_("Cancel"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "btn_cancel", btn_cancel);
  gtk_widget_show (btn_cancel);
  gtk_container_add (GTK_CONTAINER (hbuttonbox2), btn_cancel);
  gtk_signal_connect (GTK_OBJECT (btn_cancel), "clicked",
                      GTK_SIGNAL_FUNC (hnt_zn_cancel_clicked),
                      NULL);

  btn_help = gtk_button_new_with_label (_("Help"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_hint_zone), "btn_help", btn_help);
  gtk_widget_show (btn_help);
  gtk_container_add (GTK_CONTAINER (hbuttonbox2), btn_help);
  gtk_signal_connect (GTK_OBJECT (btn_help), "clicked",
                      GTK_SIGNAL_FUNC (hnt_zn_help_clicked),
                      NULL);

  return dlg_new_hint_zone;
}

GtkWidget*
create_dlg_new_slave_zone ()
{
  GtkWidget *dlg_new_slave_zone;
  GtkWidget *dialog_vbox4;
  GtkWidget *vbox5;
  GtkWidget *fr_name_servers;
  GtkWidget *vbox6;
  GtkWidget *cl_srvr_list;
  GtkWidget *label1;
  GtkWidget *hbuttonbox14;
  GtkWidget *btn_add_srvr;
  GtkWidget *btn_del_srvr;
  GtkWidget *fr_zone_backups;
  GtkWidget *hbox4;
  GtkWidget *lbl_zone_file;
  GtkWidget *en_zone_file;
  GtkWidget *fr_acl;
  GtkWidget *hbuttonbox7;
  GtkWidget *btn_update;
  GtkWidget *btn_query;
  GtkWidget *btn_transfer;
  GtkWidget *fr_zone_xfers;
  GtkWidget *hbox5;
  GtkWidget *lbl_longest_xfer;
  GtkWidget *en_max_xfer_time;
  GtkWidget *lbl_seconds;
  GtkWidget *fr_notify;
  GtkWidget *hbuttonbox8;
  GSList *hbuttonbox8_group = NULL;
  GtkWidget *rb_notify_on;
  GtkWidget *rb_notify_off;
  GtkWidget *btn_also_notify;
  GtkWidget *fr_check_name;
  GtkWidget *hbuttonbox6;
  GSList *hbuttonbox6_group = NULL;
  GtkWidget *rb_ck_warn;
  GtkWidget *rb_ck_fail;
  GtkWidget *rb_ck_ignore;
  GtkWidget *dialog_action_area4;
  GtkWidget *hbuttonbox4;
  GtkWidget *btn_ok;
  GtkWidget *btn_cancel;
  GtkWidget *btn_help;

  dlg_new_slave_zone = gtk_dialog_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "dlg_new_slave_zone", dlg_new_slave_zone);
  gtk_window_set_title (GTK_WINDOW (dlg_new_slave_zone), _("New/Edit Slave/Stub Zone"));
  gtk_window_set_policy (GTK_WINDOW (dlg_new_slave_zone), TRUE, TRUE, FALSE);

  dialog_vbox4 = GTK_DIALOG (dlg_new_slave_zone)->vbox;
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "dialog_vbox4", dialog_vbox4);
  gtk_widget_show (dialog_vbox4);

  vbox5 = gtk_vbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "vbox5", vbox5);
  gtk_widget_show (vbox5);
  gtk_box_pack_start (GTK_BOX (dialog_vbox4), vbox5, TRUE, TRUE, 0);

  fr_name_servers = gtk_frame_new (_("Name Servers"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "fr_name_servers", fr_name_servers);
  gtk_widget_show (fr_name_servers);
  gtk_box_pack_start (GTK_BOX (vbox5), fr_name_servers, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (fr_name_servers), 3);

  vbox6 = gtk_vbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "vbox6", vbox6);
  gtk_widget_show (vbox6);
  gtk_container_add (GTK_CONTAINER (fr_name_servers), vbox6);

  cl_srvr_list = gtk_clist_new (1);
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "cl_srvr_list", cl_srvr_list);
  gtk_widget_show (cl_srvr_list);
  gtk_box_pack_start (GTK_BOX (vbox6), cl_srvr_list, TRUE, TRUE, 0);
  gtk_widget_set_usize (cl_srvr_list, -1, 50);
  gtk_clist_set_column_width (GTK_CLIST (cl_srvr_list), 0, 80);
  gtk_clist_column_titles_hide (GTK_CLIST (cl_srvr_list));

  label1 = gtk_label_new (_("label1"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "label1", label1);
  gtk_widget_show (label1);
  gtk_clist_set_column_widget (GTK_CLIST (cl_srvr_list), 0, label1);

  hbuttonbox14 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "hbuttonbox14", hbuttonbox14);
  gtk_widget_show (hbuttonbox14);
  gtk_box_pack_start (GTK_BOX (vbox6), hbuttonbox14, TRUE, TRUE, 0);

  btn_add_srvr = gtk_button_new_with_label (_("Add..."));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "btn_add_srvr", btn_add_srvr);
  gtk_widget_show (btn_add_srvr);
  gtk_container_add (GTK_CONTAINER (hbuttonbox14), btn_add_srvr);
  gtk_signal_connect (GTK_OBJECT (btn_add_srvr), "clicked",
                      GTK_SIGNAL_FUNC (slv_zn_add_srvr_clicked),
                      NULL);

  btn_del_srvr = gtk_button_new_with_label (_("Delete"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "btn_del_srvr", btn_del_srvr);
  gtk_widget_show (btn_del_srvr);
  gtk_container_add (GTK_CONTAINER (hbuttonbox14), btn_del_srvr);
  gtk_signal_connect (GTK_OBJECT (btn_del_srvr), "clicked",
                      GTK_SIGNAL_FUNC (slv_zn_del_srvr_clicked),
                      NULL);

  fr_zone_backups = gtk_frame_new (_("Zone Backups"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "fr_zone_backups", fr_zone_backups);
  gtk_widget_show (fr_zone_backups);
  gtk_box_pack_start (GTK_BOX (vbox5), fr_zone_backups, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (fr_zone_backups), 3);

  hbox4 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "hbox4", hbox4);
  gtk_widget_show (hbox4);
  gtk_container_add (GTK_CONTAINER (fr_zone_backups), hbox4);

  lbl_zone_file = gtk_label_new (_("File"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "lbl_zone_file", lbl_zone_file);
  gtk_widget_show (lbl_zone_file);
  gtk_box_pack_start (GTK_BOX (hbox4), lbl_zone_file, TRUE, TRUE, 0);

  en_zone_file = gtk_entry_new_with_max_length (255);
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "en_zone_file", en_zone_file);
  gtk_widget_show (en_zone_file);
  gtk_box_pack_start (GTK_BOX (hbox4), en_zone_file, TRUE, TRUE, 0);

  fr_acl = gtk_frame_new (_("Access Control"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "fr_acl", fr_acl);
  gtk_widget_show (fr_acl);
  gtk_box_pack_start (GTK_BOX (vbox5), fr_acl, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (fr_acl), 3);

  hbuttonbox7 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "hbuttonbox7", hbuttonbox7);
  gtk_widget_show (hbuttonbox7);
  gtk_container_add (GTK_CONTAINER (fr_acl), hbuttonbox7);

  btn_update = gtk_button_new_with_label (_("Update..."));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "btn_update", btn_update);
  gtk_widget_show (btn_update);
  gtk_container_add (GTK_CONTAINER (hbuttonbox7), btn_update);
  gtk_signal_connect (GTK_OBJECT (btn_update), "clicked",
                      GTK_SIGNAL_FUNC (slv_zn_acl_update_clicked),
                      NULL);

  btn_query = gtk_button_new_with_label (_("Query..."));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "btn_query", btn_query);
  gtk_widget_show (btn_query);
  gtk_container_add (GTK_CONTAINER (hbuttonbox7), btn_query);
  gtk_signal_connect (GTK_OBJECT (btn_query), "clicked",
                      GTK_SIGNAL_FUNC (slv_zn_acl_query_clicked),
                      NULL);

  btn_transfer = gtk_button_new_with_label (_("Transfer..."));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "btn_transfer", btn_transfer);
  gtk_widget_show (btn_transfer);
  gtk_container_add (GTK_CONTAINER (hbuttonbox7), btn_transfer);
  gtk_signal_connect (GTK_OBJECT (btn_transfer), "clicked",
                      GTK_SIGNAL_FUNC (slv_zn_acl_transfer_clicked),
                      NULL);

  fr_zone_xfers = gtk_frame_new (_("Zone Transfers"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "fr_zone_xfers", fr_zone_xfers);
  gtk_widget_show (fr_zone_xfers);
  gtk_box_pack_start (GTK_BOX (vbox5), fr_zone_xfers, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (fr_zone_xfers), 3);

  hbox5 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "hbox5", hbox5);
  gtk_widget_show (hbox5);
  gtk_container_add (GTK_CONTAINER (fr_zone_xfers), hbox5);

  lbl_longest_xfer = gtk_label_new (_("Longest Transfer"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "lbl_longest_xfer", lbl_longest_xfer);
  gtk_widget_show (lbl_longest_xfer);
  gtk_box_pack_start (GTK_BOX (hbox5), lbl_longest_xfer, TRUE, TRUE, 0);

  en_max_xfer_time = gtk_entry_new_with_max_length (6);
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "en_max_xfer_time", en_max_xfer_time);
  gtk_widget_show (en_max_xfer_time);
  gtk_box_pack_start (GTK_BOX (hbox5), en_max_xfer_time, TRUE, TRUE, 0);

  lbl_seconds = gtk_label_new (_("(seconds)"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "lbl_seconds", lbl_seconds);
  gtk_widget_show (lbl_seconds);
  gtk_box_pack_start (GTK_BOX (hbox5), lbl_seconds, TRUE, TRUE, 0);

  fr_notify = gtk_frame_new (_("DNS NOTIFY"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "fr_notify", fr_notify);
  gtk_widget_show (fr_notify);
  gtk_box_pack_start (GTK_BOX (vbox5), fr_notify, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (fr_notify), 3);

  hbuttonbox8 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "hbuttonbox8", hbuttonbox8);
  gtk_widget_show (hbuttonbox8);
  gtk_container_add (GTK_CONTAINER (fr_notify), hbuttonbox8);

  rb_notify_on = gtk_radio_button_new_with_label (hbuttonbox8_group, _("On"));
  hbuttonbox8_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_notify_on));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "rb_notify_on", rb_notify_on);
  gtk_widget_show (rb_notify_on);
  gtk_container_add (GTK_CONTAINER (hbuttonbox8), rb_notify_on);

  rb_notify_off = gtk_radio_button_new_with_label (hbuttonbox8_group, _("Off"));
  hbuttonbox8_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_notify_off));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "rb_notify_off", rb_notify_off);
  gtk_widget_show (rb_notify_off);
  gtk_container_add (GTK_CONTAINER (hbuttonbox8), rb_notify_off);

  btn_also_notify = gtk_button_new_with_label (_("Also Notify..."));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "btn_also_notify", btn_also_notify);
  gtk_widget_show (btn_also_notify);
  gtk_container_add (GTK_CONTAINER (hbuttonbox8), btn_also_notify);
  gtk_signal_connect (GTK_OBJECT (btn_also_notify), "clicked",
                      GTK_SIGNAL_FUNC (slv_zn_also_notify_clicked),
                      NULL);

  fr_check_name = gtk_frame_new (_("Check Names"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "fr_check_name", fr_check_name);
  gtk_widget_show (fr_check_name);
  gtk_box_pack_start (GTK_BOX (vbox5), fr_check_name, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (fr_check_name), 3);

  hbuttonbox6 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "hbuttonbox6", hbuttonbox6);
  gtk_widget_show (hbuttonbox6);
  gtk_container_add (GTK_CONTAINER (fr_check_name), hbuttonbox6);

  rb_ck_warn = gtk_radio_button_new_with_label (hbuttonbox6_group, _("Warn"));
  hbuttonbox6_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_ck_warn));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "rb_ck_warn", rb_ck_warn);
  gtk_widget_show (rb_ck_warn);
  gtk_container_add (GTK_CONTAINER (hbuttonbox6), rb_ck_warn);

  rb_ck_fail = gtk_radio_button_new_with_label (hbuttonbox6_group, _("Fail"));
  hbuttonbox6_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_ck_fail));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "rb_ck_fail", rb_ck_fail);
  gtk_widget_show (rb_ck_fail);
  gtk_container_add (GTK_CONTAINER (hbuttonbox6), rb_ck_fail);

  rb_ck_ignore = gtk_radio_button_new_with_label (hbuttonbox6_group, _("Ignore"));
  hbuttonbox6_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_ck_ignore));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "rb_ck_ignore", rb_ck_ignore);
  gtk_widget_show (rb_ck_ignore);
  gtk_container_add (GTK_CONTAINER (hbuttonbox6), rb_ck_ignore);

  dialog_action_area4 = GTK_DIALOG (dlg_new_slave_zone)->action_area;
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "dialog_action_area4", dialog_action_area4);
  gtk_widget_show (dialog_action_area4);
  gtk_container_border_width (GTK_CONTAINER (dialog_action_area4), 10);

  hbuttonbox4 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "hbuttonbox4", hbuttonbox4);
  gtk_widget_show (hbuttonbox4);
  gtk_box_pack_start (GTK_BOX (dialog_action_area4), hbuttonbox4, TRUE, TRUE, 0);

  btn_ok = gtk_button_new_with_label (_("Ok"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "btn_ok", btn_ok);
  gtk_widget_show (btn_ok);
  gtk_container_add (GTK_CONTAINER (hbuttonbox4), btn_ok);
  GTK_WIDGET_SET_FLAGS (btn_ok, GTK_CAN_DEFAULT);
  gtk_widget_grab_default (btn_ok);
  gtk_signal_connect (GTK_OBJECT (btn_ok), "clicked",
                      GTK_SIGNAL_FUNC (slv_zn_ok_clicked),
                      NULL);

  btn_cancel = gtk_button_new_with_label (_("Cancel"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "btn_cancel", btn_cancel);
  gtk_widget_show (btn_cancel);
  gtk_container_add (GTK_CONTAINER (hbuttonbox4), btn_cancel);
  gtk_signal_connect (GTK_OBJECT (btn_cancel), "clicked",
                      GTK_SIGNAL_FUNC (slv_zn_cancel_clicked),
                      NULL);

  btn_help = gtk_button_new_with_label (_("Help"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_slave_zone), "btn_help", btn_help);
  gtk_widget_show (btn_help);
  gtk_container_add (GTK_CONTAINER (hbuttonbox4), btn_help);
  gtk_signal_connect (GTK_OBJECT (btn_help), "clicked",
                      GTK_SIGNAL_FUNC (slv_zn_help_clicked),
                      NULL);

  return dlg_new_slave_zone;
}

GtkWidget*
create_dlg_new_master_zone ()
{
  GtkWidget *dlg_new_master_zone;
  GtkWidget *dialog_vbox4;
  GtkWidget *vbox5;
  GtkWidget *fr_record_edit;
  GtkWidget *btn_edit_rr;
  GtkWidget *fr_zone_backups;
  GtkWidget *hbox4;
  GtkWidget *lbl_zone_file;
  GtkWidget *en_zone_file;
  GtkWidget *fr_acl;
  GtkWidget *hbuttonbox7;
  GtkWidget *btn_update;
  GtkWidget *btn_query;
  GtkWidget *btn_transfer;
  GtkWidget *fr_zone_xfers;
  GtkWidget *hbox5;
  GtkWidget *lbl_longest_xfer;
  GtkWidget *en_max_xfer_time;
  GtkWidget *lbl_seconds;
  GtkWidget *fr_notify;
  GtkWidget *hbuttonbox8;
  GSList *hbuttonbox8_group = NULL;
  GtkWidget *rb_notify_on;
  GtkWidget *rb_notify_off;
  GtkWidget *btn_also_notify;
  GtkWidget *fr_check_name;
  GtkWidget *hbuttonbox6;
  GSList *hbuttonbox6_group = NULL;
  GtkWidget *rb_ck_warn;
  GtkWidget *rb_ck_fail;
  GtkWidget *rb_ck_ignore;
  GtkWidget *dialog_action_area4;
  GtkWidget *hbuttonbox4;
  GtkWidget *btn_ok;
  GtkWidget *btn_cancel;
  GtkWidget *btn_help;

  dlg_new_master_zone = gtk_dialog_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "dlg_new_master_zone", dlg_new_master_zone);
  gtk_window_set_title (GTK_WINDOW (dlg_new_master_zone), _("New/Edit Master Zone"));
  gtk_window_set_policy (GTK_WINDOW (dlg_new_master_zone), TRUE, TRUE, FALSE);

  dialog_vbox4 = GTK_DIALOG (dlg_new_master_zone)->vbox;
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "dialog_vbox4", dialog_vbox4);
  gtk_widget_show (dialog_vbox4);

  vbox5 = gtk_vbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "vbox5", vbox5);
  gtk_widget_show (vbox5);
  gtk_box_pack_start (GTK_BOX (dialog_vbox4), vbox5, TRUE, TRUE, 0);

  fr_record_edit = gtk_frame_new (_("Zone Records"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "fr_record_edit", fr_record_edit);
  gtk_widget_show (fr_record_edit);
  gtk_box_pack_start (GTK_BOX (vbox5), fr_record_edit, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (fr_record_edit), 3);

  btn_edit_rr = gtk_button_new_with_label (_("Edit Zone Resource Records..."));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "btn_edit_rr", btn_edit_rr);
  gtk_widget_show (btn_edit_rr);
  gtk_container_add (GTK_CONTAINER (fr_record_edit), btn_edit_rr);
  gtk_signal_connect (GTK_OBJECT (btn_edit_rr), "clicked",
                      GTK_SIGNAL_FUNC (mst_zn_edit_rr_clicked),
                      NULL);

  fr_zone_backups = gtk_frame_new (_("On-disk Storage"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "fr_zone_backups", fr_zone_backups);
  gtk_widget_show (fr_zone_backups);
  gtk_box_pack_start (GTK_BOX (vbox5), fr_zone_backups, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (fr_zone_backups), 3);

  hbox4 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "hbox4", hbox4);
  gtk_widget_show (hbox4);
  gtk_container_add (GTK_CONTAINER (fr_zone_backups), hbox4);

  lbl_zone_file = gtk_label_new (_("Zone File"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "lbl_zone_file", lbl_zone_file);
  gtk_widget_show (lbl_zone_file);
  gtk_box_pack_start (GTK_BOX (hbox4), lbl_zone_file, TRUE, TRUE, 0);

  en_zone_file = gtk_entry_new_with_max_length (255);
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "en_zone_file", en_zone_file);
  gtk_widget_show (en_zone_file);
  gtk_box_pack_start (GTK_BOX (hbox4), en_zone_file, TRUE, TRUE, 0);

  fr_acl = gtk_frame_new (_("Access Control"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "fr_acl", fr_acl);
  gtk_widget_show (fr_acl);
  gtk_box_pack_start (GTK_BOX (vbox5), fr_acl, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (fr_acl), 3);

  hbuttonbox7 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "hbuttonbox7", hbuttonbox7);
  gtk_widget_show (hbuttonbox7);
  gtk_container_add (GTK_CONTAINER (fr_acl), hbuttonbox7);

  btn_update = gtk_button_new_with_label (_("Update..."));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "btn_update", btn_update);
  gtk_widget_show (btn_update);
  gtk_container_add (GTK_CONTAINER (hbuttonbox7), btn_update);
  gtk_signal_connect (GTK_OBJECT (btn_update), "clicked",
                      GTK_SIGNAL_FUNC (mst_zn_acl_update_clicked),
                      NULL);

  btn_query = gtk_button_new_with_label (_("Query..."));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "btn_query", btn_query);
  gtk_widget_show (btn_query);
  gtk_container_add (GTK_CONTAINER (hbuttonbox7), btn_query);
  gtk_signal_connect (GTK_OBJECT (btn_query), "clicked",
                      GTK_SIGNAL_FUNC (mst_zn_acl_query_clicked),
                      NULL);

  btn_transfer = gtk_button_new_with_label (_("Transfer..."));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "btn_transfer", btn_transfer);
  gtk_widget_show (btn_transfer);
  gtk_container_add (GTK_CONTAINER (hbuttonbox7), btn_transfer);
  gtk_signal_connect (GTK_OBJECT (btn_transfer), "clicked",
                      GTK_SIGNAL_FUNC (mst_zn_acl_transfer_clicked),
                      NULL);

  fr_zone_xfers = gtk_frame_new (_("Zone Transfers"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "fr_zone_xfers", fr_zone_xfers);
  gtk_widget_show (fr_zone_xfers);
  gtk_box_pack_start (GTK_BOX (vbox5), fr_zone_xfers, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (fr_zone_xfers), 3);

  hbox5 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "hbox5", hbox5);
  gtk_widget_show (hbox5);
  gtk_container_add (GTK_CONTAINER (fr_zone_xfers), hbox5);

  lbl_longest_xfer = gtk_label_new (_("Longest Transfer"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "lbl_longest_xfer", lbl_longest_xfer);
  gtk_widget_show (lbl_longest_xfer);
  gtk_box_pack_start (GTK_BOX (hbox5), lbl_longest_xfer, TRUE, TRUE, 0);

  en_max_xfer_time = gtk_entry_new_with_max_length (6);
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "en_max_xfer_time", en_max_xfer_time);
  gtk_widget_show (en_max_xfer_time);
  gtk_box_pack_start (GTK_BOX (hbox5), en_max_xfer_time, TRUE, TRUE, 0);

  lbl_seconds = gtk_label_new (_("(seconds)"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "lbl_seconds", lbl_seconds);
  gtk_widget_show (lbl_seconds);
  gtk_box_pack_start (GTK_BOX (hbox5), lbl_seconds, TRUE, TRUE, 0);

  fr_notify = gtk_frame_new (_("DNS NOTIFY"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "fr_notify", fr_notify);
  gtk_widget_show (fr_notify);
  gtk_box_pack_start (GTK_BOX (vbox5), fr_notify, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (fr_notify), 3);

  hbuttonbox8 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "hbuttonbox8", hbuttonbox8);
  gtk_widget_show (hbuttonbox8);
  gtk_container_add (GTK_CONTAINER (fr_notify), hbuttonbox8);

  rb_notify_on = gtk_radio_button_new_with_label (hbuttonbox8_group, _("On"));
  hbuttonbox8_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_notify_on));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "rb_notify_on", rb_notify_on);
  gtk_widget_show (rb_notify_on);
  gtk_container_add (GTK_CONTAINER (hbuttonbox8), rb_notify_on);

  rb_notify_off = gtk_radio_button_new_with_label (hbuttonbox8_group, _("Off"));
  hbuttonbox8_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_notify_off));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "rb_notify_off", rb_notify_off);
  gtk_widget_show (rb_notify_off);
  gtk_container_add (GTK_CONTAINER (hbuttonbox8), rb_notify_off);

  btn_also_notify = gtk_button_new_with_label (_("Also Notify..."));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "btn_also_notify", btn_also_notify);
  gtk_widget_show (btn_also_notify);
  gtk_container_add (GTK_CONTAINER (hbuttonbox8), btn_also_notify);
  gtk_signal_connect (GTK_OBJECT (btn_also_notify), "clicked",
                      GTK_SIGNAL_FUNC (mst_zn_also_notify_clicked),
                      NULL);

  fr_check_name = gtk_frame_new (_("Check Names"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "fr_check_name", fr_check_name);
  gtk_widget_show (fr_check_name);
  gtk_box_pack_start (GTK_BOX (vbox5), fr_check_name, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (fr_check_name), 3);

  hbuttonbox6 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "hbuttonbox6", hbuttonbox6);
  gtk_widget_show (hbuttonbox6);
  gtk_container_add (GTK_CONTAINER (fr_check_name), hbuttonbox6);

  rb_ck_warn = gtk_radio_button_new_with_label (hbuttonbox6_group, _("Warn"));
  hbuttonbox6_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_ck_warn));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "rb_ck_warn", rb_ck_warn);
  gtk_widget_show (rb_ck_warn);
  gtk_container_add (GTK_CONTAINER (hbuttonbox6), rb_ck_warn);

  rb_ck_fail = gtk_radio_button_new_with_label (hbuttonbox6_group, _("Fail"));
  hbuttonbox6_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_ck_fail));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "rb_ck_fail", rb_ck_fail);
  gtk_widget_show (rb_ck_fail);
  gtk_container_add (GTK_CONTAINER (hbuttonbox6), rb_ck_fail);

  rb_ck_ignore = gtk_radio_button_new_with_label (hbuttonbox6_group, _("Ignore"));
  hbuttonbox6_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_ck_ignore));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "rb_ck_ignore", rb_ck_ignore);
  gtk_widget_show (rb_ck_ignore);
  gtk_container_add (GTK_CONTAINER (hbuttonbox6), rb_ck_ignore);

  dialog_action_area4 = GTK_DIALOG (dlg_new_master_zone)->action_area;
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "dialog_action_area4", dialog_action_area4);
  gtk_widget_show (dialog_action_area4);
  gtk_container_border_width (GTK_CONTAINER (dialog_action_area4), 10);

  hbuttonbox4 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "hbuttonbox4", hbuttonbox4);
  gtk_widget_show (hbuttonbox4);
  gtk_box_pack_start (GTK_BOX (dialog_action_area4), hbuttonbox4, TRUE, TRUE, 0);

  btn_ok = gtk_button_new_with_label (_("Ok"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "btn_ok", btn_ok);
  gtk_widget_show (btn_ok);
  gtk_container_add (GTK_CONTAINER (hbuttonbox4), btn_ok);
  GTK_WIDGET_SET_FLAGS (btn_ok, GTK_CAN_DEFAULT);
  gtk_widget_grab_default (btn_ok);
  gtk_signal_connect (GTK_OBJECT (btn_ok), "clicked",
                      GTK_SIGNAL_FUNC (mst_zn_ok_clicked),
                      NULL);

  btn_cancel = gtk_button_new_with_label (_("Cancel"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "btn_cancel", btn_cancel);
  gtk_widget_show (btn_cancel);
  gtk_container_add (GTK_CONTAINER (hbuttonbox4), btn_cancel);
  gtk_signal_connect (GTK_OBJECT (btn_cancel), "clicked",
                      GTK_SIGNAL_FUNC (mst_zn_cancel_clicked),
                      NULL);

  btn_help = gtk_button_new_with_label (_("Help"));
  gtk_object_set_data (GTK_OBJECT (dlg_new_master_zone), "btn_help", btn_help);
  gtk_widget_show (btn_help);
  gtk_container_add (GTK_CONTAINER (hbuttonbox4), btn_help);
  gtk_signal_connect (GTK_OBJECT (btn_help), "clicked",
                      GTK_SIGNAL_FUNC (mst_zn_help_clicked),
                      NULL);

  return dlg_new_master_zone;
}

GtkWidget*
create_dlg_server_options ()
{
  GtkWidget *dlg_server_options;
  GtkWidget *dialog_vbox1;
  GtkWidget *nb_server_options;
  GtkWidget *table1;
  GtkWidget *label10;
  GtkWidget *label11;
  GtkWidget *label12;
  GtkWidget *label13;
  GtkWidget *label14;
  GtkWidget *label15;
  GtkWidget *ent_master_dir;
  GtkWidget *ent_named_xfer;
  GtkWidget *ent_dump_file;
  GtkWidget *ent_mem_stats;
  GtkWidget *ent_pid_file;
  GtkWidget *ent_stats_file;
  GtkWidget *table2;
  GtkWidget *ck_auth_nx_domain;
  GtkWidget *ck_dealloc_on_exit;
  GtkWidget *ck_fake_iquery;
  GtkWidget *ck_fetch_glue;
  GtkWidget *ck_recursion;
  GtkWidget *ck_send_notify;
  GtkWidget *ck_multiple_cnames;
  GtkWidget *ck_host_stats;
  GtkWidget *table3;
  GtkWidget *label16;
  GtkWidget *label17;
  GtkWidget *label18;
  GtkWidget *label19;
  GtkWidget *hbox2;
  GSList *hbox2_group = NULL;
  GtkWidget *rb_core_def;
  GtkWidget *rb_core_unlim;
  GtkWidget *ent_core;
  GtkWidget *hbox3;
  GSList *hbox3_group = NULL;
  GtkWidget *rb_data_def;
  GtkWidget *rb_data_unlim;
  GtkWidget *ent_data;
  GtkWidget *hbox4;
  GSList *hbox4_group = NULL;
  GtkWidget *rb_stack_def;
  GtkWidget *rb_stack_unlim;
  GtkWidget *ent_stack;
  GtkWidget *hbox5;
  GSList *hbox5_group = NULL;
  GtkWidget *rb_files_def;
  GtkWidget *rb_files_unlim;
  GtkWidget *ent_files;
  GtkWidget *table4;
  GtkWidget *label20;
  GtkWidget *label21;
  GtkWidget *label22;
  GtkWidget *ent_cleaning_interval;
  GtkWidget *ent_int_scan;
  GtkWidget *entry13;
  GtkWidget *vbox2;
  GtkWidget *hbox6;
  GtkWidget *label23;
  GSList *hbox6_group = NULL;
  GtkWidget *rb_for_first;
  GtkWidget *rb_for_only;
  GtkWidget *vbox3;
  GtkWidget *hbox8;
  GtkWidget *label24;
  GtkWidget *ent_for_ip;
  GtkWidget *clist_fwd_ips;
  GtkWidget *label32;
  GtkWidget *label33;
  GtkWidget *hbox7;
  GtkWidget *hbuttonbox13;
  GtkWidget *btn_fwd_add;
  GtkWidget *btn_fwd_del;
  GtkWidget *vbox8;
  GtkWidget *frame2;
  GtkWidget *hbuttonbox10;
  GSList *hbuttonbox10_group = NULL;
  GtkWidget *rb_nm_ck_master_warn;
  GtkWidget *rb_nm_ck_master_fail;
  GtkWidget *rb_nm_ck_master_ignore;
  GtkWidget *frame3;
  GtkWidget *hbuttonbox11;
  GSList *hbuttonbox11_group = NULL;
  GtkWidget *rb_nm_ck_slave_warn;
  GtkWidget *rb_nm_ck_slave_fail;
  GtkWidget *rb_nm_ck_slave_ignore;
  GtkWidget *frame4;
  GtkWidget *hbuttonbox12;
  GSList *hbuttonbox12_group = NULL;
  GtkWidget *rb_nm_ck_rsp_warn;
  GtkWidget *rb_nm_ck_rsp_fail;
  GtkWidget *rb_nm_ck_rsp_ignore;
  GtkWidget *table6;
  GtkWidget *vbox4;
  GtkWidget *button6;
  GtkWidget *button7;
  GtkWidget *vbox5;
  GtkWidget *button8;
  GtkWidget *button9;
  GtkWidget *vbox6;
  GtkWidget *hbox9;
  GtkWidget *label30;
  GtkWidget *entry15;
  GtkWidget *list1;
  GtkWidget *vbox7;
  GtkWidget *hbox10;
  GtkWidget *label31;
  GtkWidget *entry16;
  GtkWidget *list2;
  GtkWidget *label3;
  GtkWidget *label4;
  GtkWidget *label5;
  GtkWidget *label6;
  GtkWidget *label7;
  GtkWidget *label8;
  GtkWidget *label9;
  GtkWidget *dialog_action_area1;
  GtkWidget *hbuttonbox9;
  GtkWidget *btn_ok;
  GtkWidget *btn_cancel;
  GtkWidget *btn_help;

  dlg_server_options = gtk_dialog_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "dlg_server_options", dlg_server_options);
  gtk_signal_connect (GTK_OBJECT (dlg_server_options), "destroy",
                      GTK_SIGNAL_FUNC (on_dlg_server_options_destroy),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (dlg_server_options), "delete_event",
                      GTK_SIGNAL_FUNC (on_dlg_server_options_delete_event),
                      NULL);
  gtk_window_set_title (GTK_WINDOW (dlg_server_options), _("Server Options"));

  dialog_vbox1 = GTK_DIALOG (dlg_server_options)->vbox;
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "dialog_vbox1", dialog_vbox1);
  gtk_widget_show (dialog_vbox1);

  nb_server_options = gtk_notebook_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "nb_server_options", nb_server_options);
  gtk_widget_show (nb_server_options);
  gtk_box_pack_start (GTK_BOX (dialog_vbox1), nb_server_options, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (nb_server_options), 10);

  table1 = gtk_table_new (6, 2, FALSE);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "table1", table1);
  gtk_widget_show (table1);
  gtk_container_add (GTK_CONTAINER (nb_server_options), table1);

  label10 = gtk_label_new (_("Master directory"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label10", label10);
  gtk_widget_show (label10);
  gtk_table_attach (GTK_TABLE (table1), label10, 0, 1, 0, 1,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_label_set_justify (GTK_LABEL (label10), GTK_JUSTIFY_RIGHT);

  label11 = gtk_label_new (_("named.xfer program"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label11", label11);
  gtk_widget_show (label11);
  gtk_table_attach (GTK_TABLE (table1), label11, 0, 1, 1, 2,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_label_set_justify (GTK_LABEL (label11), GTK_JUSTIFY_RIGHT);

  label12 = gtk_label_new (_("Dump file"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label12", label12);
  gtk_widget_show (label12);
  gtk_table_attach (GTK_TABLE (table1), label12, 0, 1, 2, 3,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_label_set_justify (GTK_LABEL (label12), GTK_JUSTIFY_RIGHT);

  label13 = gtk_label_new (_("Mem stats file"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label13", label13);
  gtk_widget_show (label13);
  gtk_table_attach (GTK_TABLE (table1), label13, 0, 1, 3, 4,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_label_set_justify (GTK_LABEL (label13), GTK_JUSTIFY_RIGHT);

  label14 = gtk_label_new (_("PID file"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label14", label14);
  gtk_widget_show (label14);
  gtk_table_attach (GTK_TABLE (table1), label14, 0, 1, 4, 5,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_label_set_justify (GTK_LABEL (label14), GTK_JUSTIFY_RIGHT);

  label15 = gtk_label_new (_("Statistics file"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label15", label15);
  gtk_widget_show (label15);
  gtk_table_attach (GTK_TABLE (table1), label15, 0, 1, 5, 6,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_label_set_justify (GTK_LABEL (label15), GTK_JUSTIFY_RIGHT);

  ent_master_dir = gtk_entry_new_with_max_length (128);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ent_master_dir", ent_master_dir);
  gtk_widget_show (ent_master_dir);
  gtk_table_attach (GTK_TABLE (table1), ent_master_dir, 1, 2, 0, 1,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ent_named_xfer = gtk_entry_new_with_max_length (128);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ent_named_xfer", ent_named_xfer);
  gtk_widget_show (ent_named_xfer);
  gtk_table_attach (GTK_TABLE (table1), ent_named_xfer, 1, 2, 1, 2,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ent_dump_file = gtk_entry_new_with_max_length (128);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ent_dump_file", ent_dump_file);
  gtk_widget_show (ent_dump_file);
  gtk_table_attach (GTK_TABLE (table1), ent_dump_file, 1, 2, 2, 3,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ent_mem_stats = gtk_entry_new_with_max_length (128);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ent_mem_stats", ent_mem_stats);
  gtk_widget_show (ent_mem_stats);
  gtk_table_attach (GTK_TABLE (table1), ent_mem_stats, 1, 2, 3, 4,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ent_pid_file = gtk_entry_new_with_max_length (128);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ent_pid_file", ent_pid_file);
  gtk_widget_show (ent_pid_file);
  gtk_table_attach (GTK_TABLE (table1), ent_pid_file, 1, 2, 4, 5,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ent_stats_file = gtk_entry_new_with_max_length (128);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ent_stats_file", ent_stats_file);
  gtk_widget_show (ent_stats_file);
  gtk_table_attach (GTK_TABLE (table1), ent_stats_file, 1, 2, 5, 6,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  table2 = gtk_table_new (4, 2, FALSE);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "table2", table2);
  gtk_widget_show (table2);
  gtk_container_add (GTK_CONTAINER (nb_server_options), table2);

  ck_auth_nx_domain = gtk_check_button_new_with_label (_("Auth NX Domain"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ck_auth_nx_domain", ck_auth_nx_domain);
  gtk_widget_show (ck_auth_nx_domain);
  gtk_table_attach (GTK_TABLE (table2), ck_auth_nx_domain, 0, 1, 0, 1,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ck_dealloc_on_exit = gtk_check_button_new_with_label (_("Dealloc on exit"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ck_dealloc_on_exit", ck_dealloc_on_exit);
  gtk_widget_show (ck_dealloc_on_exit);
  gtk_table_attach (GTK_TABLE (table2), ck_dealloc_on_exit, 0, 1, 1, 2,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ck_fake_iquery = gtk_check_button_new_with_label (_("Fake IQUERY"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ck_fake_iquery", ck_fake_iquery);
  gtk_widget_show (ck_fake_iquery);
  gtk_table_attach (GTK_TABLE (table2), ck_fake_iquery, 0, 1, 2, 3,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ck_fetch_glue = gtk_check_button_new_with_label (_("Fetch glue records"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ck_fetch_glue", ck_fetch_glue);
  gtk_widget_show (ck_fetch_glue);
  gtk_table_attach (GTK_TABLE (table2), ck_fetch_glue, 0, 1, 3, 4,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ck_recursion = gtk_check_button_new_with_label (_("Recursive searches"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ck_recursion", ck_recursion);
  gtk_widget_show (ck_recursion);
  gtk_table_attach (GTK_TABLE (table2), ck_recursion, 1, 2, 3, 4,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ck_send_notify = gtk_check_button_new_with_label (_("Send NOTIFY messages"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ck_send_notify", ck_send_notify);
  gtk_widget_show (ck_send_notify);
  gtk_table_attach (GTK_TABLE (table2), ck_send_notify, 1, 2, 2, 3,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ck_multiple_cnames = gtk_check_button_new_with_label (_("Support multiple CNAMEs"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ck_multiple_cnames", ck_multiple_cnames);
  gtk_widget_show (ck_multiple_cnames);
  gtk_table_attach (GTK_TABLE (table2), ck_multiple_cnames, 1, 2, 1, 2,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ck_host_stats = gtk_check_button_new_with_label (_("Keep per-host statistics"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ck_host_stats", ck_host_stats);
  gtk_widget_show (ck_host_stats);
  gtk_table_attach (GTK_TABLE (table2), ck_host_stats, 1, 2, 0, 1,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  table3 = gtk_table_new (4, 2, FALSE);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "table3", table3);
  gtk_widget_show (table3);
  gtk_container_add (GTK_CONTAINER (nb_server_options), table3);

  label16 = gtk_label_new (_("Core size"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label16", label16);
  gtk_widget_show (label16);
  gtk_table_attach (GTK_TABLE (table3), label16, 0, 1, 0, 1,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  label17 = gtk_label_new (_("Data size"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label17", label17);
  gtk_widget_show (label17);
  gtk_table_attach (GTK_TABLE (table3), label17, 0, 1, 1, 2,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  label18 = gtk_label_new (_("Stack size"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label18", label18);
  gtk_widget_show (label18);
  gtk_table_attach (GTK_TABLE (table3), label18, 0, 1, 2, 3,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  label19 = gtk_label_new (_("Open files"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label19", label19);
  gtk_widget_show (label19);
  gtk_table_attach (GTK_TABLE (table3), label19, 0, 1, 3, 4,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  hbox2 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbox2", hbox2);
  gtk_widget_show (hbox2);
  gtk_table_attach (GTK_TABLE (table3), hbox2, 1, 2, 0, 1,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  rb_core_def = gtk_radio_button_new_with_label (hbox2_group, _("default"));
  hbox2_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_core_def));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_core_def", rb_core_def);
  gtk_widget_show (rb_core_def);
  gtk_box_pack_start (GTK_BOX (hbox2), rb_core_def, TRUE, TRUE, 0);

  rb_core_unlim = gtk_radio_button_new_with_label (hbox2_group, _("unlimited"));
  hbox2_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_core_unlim));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_core_unlim", rb_core_unlim);
  gtk_widget_show (rb_core_unlim);
  gtk_box_pack_start (GTK_BOX (hbox2), rb_core_unlim, TRUE, TRUE, 0);

  ent_core = gtk_entry_new_with_max_length (14);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ent_core", ent_core);
  gtk_widget_show (ent_core);
  gtk_box_pack_start (GTK_BOX (hbox2), ent_core, TRUE, TRUE, 0);

  hbox3 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbox3", hbox3);
  gtk_widget_show (hbox3);
  gtk_table_attach (GTK_TABLE (table3), hbox3, 1, 2, 1, 2,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  rb_data_def = gtk_radio_button_new_with_label (hbox3_group, _("default"));
  hbox3_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_data_def));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_data_def", rb_data_def);
  gtk_widget_show (rb_data_def);
  gtk_box_pack_start (GTK_BOX (hbox3), rb_data_def, TRUE, TRUE, 0);

  rb_data_unlim = gtk_radio_button_new_with_label (hbox3_group, _("unlimited"));
  hbox3_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_data_unlim));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_data_unlim", rb_data_unlim);
  gtk_widget_show (rb_data_unlim);
  gtk_box_pack_start (GTK_BOX (hbox3), rb_data_unlim, TRUE, TRUE, 0);

  ent_data = gtk_entry_new_with_max_length (14);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ent_data", ent_data);
  gtk_widget_show (ent_data);
  gtk_box_pack_start (GTK_BOX (hbox3), ent_data, TRUE, TRUE, 0);

  hbox4 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbox4", hbox4);
  gtk_widget_show (hbox4);
  gtk_table_attach (GTK_TABLE (table3), hbox4, 1, 2, 2, 3,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  rb_stack_def = gtk_radio_button_new_with_label (hbox4_group, _("default"));
  hbox4_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_stack_def));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_stack_def", rb_stack_def);
  gtk_widget_show (rb_stack_def);
  gtk_box_pack_start (GTK_BOX (hbox4), rb_stack_def, TRUE, TRUE, 0);

  rb_stack_unlim = gtk_radio_button_new_with_label (hbox4_group, _("unlimited"));
  hbox4_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_stack_unlim));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_stack_unlim", rb_stack_unlim);
  gtk_widget_show (rb_stack_unlim);
  gtk_box_pack_start (GTK_BOX (hbox4), rb_stack_unlim, TRUE, TRUE, 0);

  ent_stack = gtk_entry_new_with_max_length (14);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ent_stack", ent_stack);
  gtk_widget_show (ent_stack);
  gtk_box_pack_start (GTK_BOX (hbox4), ent_stack, TRUE, TRUE, 0);

  hbox5 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbox5", hbox5);
  gtk_widget_show (hbox5);
  gtk_table_attach (GTK_TABLE (table3), hbox5, 1, 2, 3, 4,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  rb_files_def = gtk_radio_button_new_with_label (hbox5_group, _("default"));
  hbox5_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_files_def));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_files_def", rb_files_def);
  gtk_widget_show (rb_files_def);
  gtk_box_pack_start (GTK_BOX (hbox5), rb_files_def, TRUE, TRUE, 0);

  rb_files_unlim = gtk_radio_button_new_with_label (hbox5_group, _("unlimited"));
  hbox5_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_files_unlim));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_files_unlim", rb_files_unlim);
  gtk_widget_show (rb_files_unlim);
  gtk_box_pack_start (GTK_BOX (hbox5), rb_files_unlim, TRUE, TRUE, 0);

  ent_files = gtk_entry_new_with_max_length (14);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ent_files", ent_files);
  gtk_widget_show (ent_files);
  gtk_box_pack_start (GTK_BOX (hbox5), ent_files, TRUE, TRUE, 0);

  table4 = gtk_table_new (3, 2, FALSE);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "table4", table4);
  gtk_widget_show (table4);
  gtk_container_add (GTK_CONTAINER (nb_server_options), table4);

  label20 = gtk_label_new (_("Cleaning interval (minutes)"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label20", label20);
  gtk_widget_show (label20);
  gtk_table_attach (GTK_TABLE (table4), label20, 0, 1, 0, 1,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  label21 = gtk_label_new (_("Interface scan interval (minutes)"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label21", label21);
  gtk_widget_show (label21);
  gtk_table_attach (GTK_TABLE (table4), label21, 0, 1, 1, 2,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  label22 = gtk_label_new (_("Statistics interval (minutes)"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label22", label22);
  gtk_widget_show (label22);
  gtk_table_attach (GTK_TABLE (table4), label22, 0, 1, 2, 3,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ent_cleaning_interval = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ent_cleaning_interval", ent_cleaning_interval);
  gtk_widget_show (ent_cleaning_interval);
  gtk_table_attach (GTK_TABLE (table4), ent_cleaning_interval, 1, 2, 0, 1,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  ent_int_scan = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ent_int_scan", ent_int_scan);
  gtk_widget_show (ent_int_scan);
  gtk_table_attach (GTK_TABLE (table4), ent_int_scan, 1, 2, 1, 2,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  entry13 = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "entry13", entry13);
  gtk_widget_show (entry13);
  gtk_table_attach (GTK_TABLE (table4), entry13, 1, 2, 2, 3,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "vbox2", vbox2);
  gtk_widget_show (vbox2);
  gtk_container_add (GTK_CONTAINER (nb_server_options), vbox2);

  hbox6 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbox6", hbox6);
  gtk_widget_show (hbox6);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox6, TRUE, TRUE, 0);

  label23 = gtk_label_new (_("Forward"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label23", label23);
  gtk_widget_show (label23);
  gtk_box_pack_start (GTK_BOX (hbox6), label23, TRUE, TRUE, 0);

  rb_for_first = gtk_radio_button_new_with_label (hbox6_group, _("first"));
  hbox6_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_for_first));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_for_first", rb_for_first);
  gtk_widget_show (rb_for_first);
  gtk_box_pack_start (GTK_BOX (hbox6), rb_for_first, TRUE, TRUE, 0);

  rb_for_only = gtk_radio_button_new_with_label (hbox6_group, _("only"));
  hbox6_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_for_only));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_for_only", rb_for_only);
  gtk_widget_show (rb_for_only);
  gtk_box_pack_start (GTK_BOX (hbox6), rb_for_only, TRUE, TRUE, 0);

  vbox3 = gtk_vbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "vbox3", vbox3);
  gtk_widget_show (vbox3);
  gtk_box_pack_start (GTK_BOX (vbox2), vbox3, TRUE, TRUE, 0);

  hbox8 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbox8", hbox8);
  gtk_widget_show (hbox8);
  gtk_box_pack_start (GTK_BOX (vbox3), hbox8, TRUE, TRUE, 0);

  label24 = gtk_label_new (_("Forward-to IP addresses:"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label24", label24);
  gtk_widget_show (label24);
  gtk_box_pack_start (GTK_BOX (hbox8), label24, TRUE, TRUE, 0);

  ent_for_ip = gtk_entry_new_with_max_length (15);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "ent_for_ip", ent_for_ip);
  gtk_widget_show (ent_for_ip);
  gtk_box_pack_start (GTK_BOX (hbox8), ent_for_ip, TRUE, TRUE, 0);

  clist_fwd_ips = gtk_clist_new (2);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "clist_fwd_ips", clist_fwd_ips);
  gtk_widget_show (clist_fwd_ips);
  gtk_box_pack_start (GTK_BOX (vbox3), clist_fwd_ips, TRUE, TRUE, 0);
  gtk_widget_set_usize (clist_fwd_ips, -1, 50);
  gtk_clist_set_column_width (GTK_CLIST (clist_fwd_ips), 0, 80);
  gtk_clist_set_column_width (GTK_CLIST (clist_fwd_ips), 1, 80);
  gtk_clist_column_titles_hide (GTK_CLIST (clist_fwd_ips));

  label32 = gtk_label_new ("");
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label32", label32);
  gtk_widget_show (label32);
  gtk_clist_set_column_widget (GTK_CLIST (clist_fwd_ips), 0, label32);

  label33 = gtk_label_new ("");
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label33", label33);
  gtk_widget_show (label33);
  gtk_clist_set_column_widget (GTK_CLIST (clist_fwd_ips), 1, label33);

  hbox7 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbox7", hbox7);
  gtk_widget_show (hbox7);
  gtk_box_pack_start (GTK_BOX (vbox3), hbox7, TRUE, TRUE, 0);

  hbuttonbox13 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbuttonbox13", hbuttonbox13);
  gtk_widget_show (hbuttonbox13);
  gtk_box_pack_start (GTK_BOX (hbox7), hbuttonbox13, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (hbuttonbox13), 3);

  btn_fwd_add = gtk_button_new_with_label (_("Add..."));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "btn_fwd_add", btn_fwd_add);
  gtk_widget_show (btn_fwd_add);
  gtk_container_add (GTK_CONTAINER (hbuttonbox13), btn_fwd_add);
  gtk_signal_connect (GTK_OBJECT (btn_fwd_add), "clicked",
                      GTK_SIGNAL_FUNC (svr_opt_fwd_add_clicked),
                      NULL);

  btn_fwd_del = gtk_button_new_with_label (_("Delete"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "btn_fwd_del", btn_fwd_del);
  gtk_widget_show (btn_fwd_del);
  gtk_container_add (GTK_CONTAINER (hbuttonbox13), btn_fwd_del);
  gtk_signal_connect (GTK_OBJECT (btn_fwd_del), "clicked",
                      GTK_SIGNAL_FUNC (svr_opt_fwd_del_clicked),
                      NULL);

  vbox8 = gtk_vbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "vbox8", vbox8);
  gtk_widget_show (vbox8);
  gtk_container_add (GTK_CONTAINER (nb_server_options), vbox8);

  frame2 = gtk_frame_new (_("Master"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "frame2", frame2);
  gtk_widget_show (frame2);
  gtk_box_pack_start (GTK_BOX (vbox8), frame2, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (frame2), 5);

  hbuttonbox10 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbuttonbox10", hbuttonbox10);
  gtk_widget_show (hbuttonbox10);
  gtk_container_add (GTK_CONTAINER (frame2), hbuttonbox10);

  rb_nm_ck_master_warn = gtk_radio_button_new_with_label (hbuttonbox10_group, _("Warn"));
  hbuttonbox10_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_nm_ck_master_warn));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_nm_ck_master_warn", rb_nm_ck_master_warn);
  gtk_widget_show (rb_nm_ck_master_warn);
  gtk_container_add (GTK_CONTAINER (hbuttonbox10), rb_nm_ck_master_warn);

  rb_nm_ck_master_fail = gtk_radio_button_new_with_label (hbuttonbox10_group, _("Fail"));
  hbuttonbox10_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_nm_ck_master_fail));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_nm_ck_master_fail", rb_nm_ck_master_fail);
  gtk_widget_show (rb_nm_ck_master_fail);
  gtk_container_add (GTK_CONTAINER (hbuttonbox10), rb_nm_ck_master_fail);

  rb_nm_ck_master_ignore = gtk_radio_button_new_with_label (hbuttonbox10_group, _("Ignore"));
  hbuttonbox10_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_nm_ck_master_ignore));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_nm_ck_master_ignore", rb_nm_ck_master_ignore);
  gtk_widget_show (rb_nm_ck_master_ignore);
  gtk_container_add (GTK_CONTAINER (hbuttonbox10), rb_nm_ck_master_ignore);

  frame3 = gtk_frame_new (_("Slave"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "frame3", frame3);
  gtk_widget_show (frame3);
  gtk_box_pack_start (GTK_BOX (vbox8), frame3, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (frame3), 5);

  hbuttonbox11 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbuttonbox11", hbuttonbox11);
  gtk_widget_show (hbuttonbox11);
  gtk_container_add (GTK_CONTAINER (frame3), hbuttonbox11);

  rb_nm_ck_slave_warn = gtk_radio_button_new_with_label (hbuttonbox11_group, _("Warn"));
  hbuttonbox11_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_nm_ck_slave_warn));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_nm_ck_slave_warn", rb_nm_ck_slave_warn);
  gtk_widget_show (rb_nm_ck_slave_warn);
  gtk_container_add (GTK_CONTAINER (hbuttonbox11), rb_nm_ck_slave_warn);

  rb_nm_ck_slave_fail = gtk_radio_button_new_with_label (hbuttonbox11_group, _("Fail"));
  hbuttonbox11_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_nm_ck_slave_fail));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_nm_ck_slave_fail", rb_nm_ck_slave_fail);
  gtk_widget_show (rb_nm_ck_slave_fail);
  gtk_container_add (GTK_CONTAINER (hbuttonbox11), rb_nm_ck_slave_fail);

  rb_nm_ck_slave_ignore = gtk_radio_button_new_with_label (hbuttonbox11_group, _("Ignore"));
  hbuttonbox11_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_nm_ck_slave_ignore));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_nm_ck_slave_ignore", rb_nm_ck_slave_ignore);
  gtk_widget_show (rb_nm_ck_slave_ignore);
  gtk_container_add (GTK_CONTAINER (hbuttonbox11), rb_nm_ck_slave_ignore);

  frame4 = gtk_frame_new (_("Response"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "frame4", frame4);
  gtk_widget_show (frame4);
  gtk_box_pack_start (GTK_BOX (vbox8), frame4, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (frame4), 5);

  hbuttonbox12 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbuttonbox12", hbuttonbox12);
  gtk_widget_show (hbuttonbox12);
  gtk_container_add (GTK_CONTAINER (frame4), hbuttonbox12);

  rb_nm_ck_rsp_warn = gtk_radio_button_new_with_label (hbuttonbox12_group, _("Warn"));
  hbuttonbox12_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_nm_ck_rsp_warn));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_nm_ck_rsp_warn", rb_nm_ck_rsp_warn);
  gtk_widget_show (rb_nm_ck_rsp_warn);
  gtk_container_add (GTK_CONTAINER (hbuttonbox12), rb_nm_ck_rsp_warn);

  rb_nm_ck_rsp_fail = gtk_radio_button_new_with_label (hbuttonbox12_group, _("Fail"));
  hbuttonbox12_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_nm_ck_rsp_fail));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_nm_ck_rsp_fail", rb_nm_ck_rsp_fail);
  gtk_widget_show (rb_nm_ck_rsp_fail);
  gtk_container_add (GTK_CONTAINER (hbuttonbox12), rb_nm_ck_rsp_fail);

  rb_nm_ck_rsp_ignore = gtk_radio_button_new_with_label (hbuttonbox12_group, _("Ignore"));
  hbuttonbox12_group = gtk_radio_button_group (GTK_RADIO_BUTTON (rb_nm_ck_rsp_ignore));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "rb_nm_ck_rsp_ignore", rb_nm_ck_rsp_ignore);
  gtk_widget_show (rb_nm_ck_rsp_ignore);
  gtk_container_add (GTK_CONTAINER (hbuttonbox12), rb_nm_ck_rsp_ignore);

  table6 = gtk_table_new (2, 2, FALSE);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "table6", table6);
  gtk_widget_show (table6);
  gtk_container_add (GTK_CONTAINER (nb_server_options), table6);

  vbox4 = gtk_vbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "vbox4", vbox4);
  gtk_widget_show (vbox4);
  gtk_table_attach (GTK_TABLE (table6), vbox4, 1, 2, 0, 1,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  button6 = gtk_button_new_with_label (_("Add"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "button6", button6);
  gtk_widget_show (button6);
  gtk_box_pack_start (GTK_BOX (vbox4), button6, TRUE, TRUE, 0);

  button7 = gtk_button_new_with_label (_("Delete"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "button7", button7);
  gtk_widget_show (button7);
  gtk_box_pack_start (GTK_BOX (vbox4), button7, TRUE, TRUE, 0);

  vbox5 = gtk_vbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "vbox5", vbox5);
  gtk_widget_show (vbox5);
  gtk_table_attach (GTK_TABLE (table6), vbox5, 1, 2, 1, 2,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  button8 = gtk_button_new_with_label (_("Add"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "button8", button8);
  gtk_widget_show (button8);
  gtk_box_pack_start (GTK_BOX (vbox5), button8, TRUE, TRUE, 0);

  button9 = gtk_button_new_with_label (_("Delete"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "button9", button9);
  gtk_widget_show (button9);
  gtk_box_pack_start (GTK_BOX (vbox5), button9, TRUE, TRUE, 0);

  vbox6 = gtk_vbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "vbox6", vbox6);
  gtk_widget_show (vbox6);
  gtk_table_attach (GTK_TABLE (table6), vbox6, 0, 1, 0, 1,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  hbox9 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbox9", hbox9);
  gtk_widget_show (hbox9);
  gtk_box_pack_start (GTK_BOX (vbox6), hbox9, TRUE, TRUE, 0);

  label30 = gtk_label_new (_("Allow queries"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label30", label30);
  gtk_widget_show (label30);
  gtk_box_pack_start (GTK_BOX (hbox9), label30, TRUE, TRUE, 0);

  entry15 = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "entry15", entry15);
  gtk_widget_show (entry15);
  gtk_box_pack_start (GTK_BOX (hbox9), entry15, TRUE, TRUE, 0);

  list1 = gtk_list_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "list1", list1);
  gtk_widget_show (list1);
  gtk_box_pack_start (GTK_BOX (vbox6), list1, TRUE, TRUE, 0);

  vbox7 = gtk_vbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "vbox7", vbox7);
  gtk_widget_show (vbox7);
  gtk_table_attach (GTK_TABLE (table6), vbox7, 0, 1, 1, 2,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  hbox10 = gtk_hbox_new (FALSE, 0);
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbox10", hbox10);
  gtk_widget_show (hbox10);
  gtk_box_pack_start (GTK_BOX (vbox7), hbox10, TRUE, TRUE, 0);

  label31 = gtk_label_new (_("Allow zone xfers"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label31", label31);
  gtk_widget_show (label31);
  gtk_box_pack_start (GTK_BOX (hbox10), label31, TRUE, TRUE, 0);

  entry16 = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "entry16", entry16);
  gtk_widget_show (entry16);
  gtk_box_pack_start (GTK_BOX (hbox10), entry16, TRUE, TRUE, 0);

  list2 = gtk_list_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "list2", list2);
  gtk_widget_show (list2);
  gtk_box_pack_start (GTK_BOX (vbox7), list2, TRUE, TRUE, 0);

  label3 = gtk_label_new (_("Paths"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label3", label3);
  gtk_widget_show (label3);
  set_notebook_tab (nb_server_options, 0, label3);

  label4 = gtk_label_new (_("Options"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label4", label4);
  gtk_widget_show (label4);
  set_notebook_tab (nb_server_options, 1, label4);

  label5 = gtk_label_new (_("Limits"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label5", label5);
  gtk_widget_show (label5);
  set_notebook_tab (nb_server_options, 2, label5);

  label6 = gtk_label_new (_("Periodic"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label6", label6);
  gtk_widget_show (label6);
  set_notebook_tab (nb_server_options, 3, label6);

  label7 = gtk_label_new (_("Forwarding"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label7", label7);
  gtk_widget_show (label7);
  set_notebook_tab (nb_server_options, 4, label7);

  label8 = gtk_label_new (_("Name Ck"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label8", label8);
  gtk_widget_show (label8);
  set_notebook_tab (nb_server_options, 5, label8);

  label9 = gtk_label_new (_("Access Ctl"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "label9", label9);
  gtk_widget_show (label9);
  set_notebook_tab (nb_server_options, 6, label9);

  dialog_action_area1 = GTK_DIALOG (dlg_server_options)->action_area;
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "dialog_action_area1", dialog_action_area1);
  gtk_widget_show (dialog_action_area1);
  gtk_container_border_width (GTK_CONTAINER (dialog_action_area1), 10);

  hbuttonbox9 = gtk_hbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "hbuttonbox9", hbuttonbox9);
  gtk_widget_show (hbuttonbox9);
  gtk_box_pack_start (GTK_BOX (dialog_action_area1), hbuttonbox9, TRUE, TRUE, 0);

  btn_ok = gtk_button_new_with_label (_("Ok"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "btn_ok", btn_ok);
  gtk_widget_show (btn_ok);
  gtk_container_add (GTK_CONTAINER (hbuttonbox9), btn_ok);
  GTK_WIDGET_SET_FLAGS (btn_ok, GTK_CAN_DEFAULT);
  gtk_widget_grab_default (btn_ok);
  gtk_signal_connect (GTK_OBJECT (btn_ok), "clicked",
                      GTK_SIGNAL_FUNC (svr_opt_ok_clicked),
                      NULL);

  btn_cancel = gtk_button_new_with_label (_("Cancel"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "btn_cancel", btn_cancel);
  gtk_widget_show (btn_cancel);
  gtk_container_add (GTK_CONTAINER (hbuttonbox9), btn_cancel);
  gtk_signal_connect (GTK_OBJECT (btn_cancel), "clicked",
                      GTK_SIGNAL_FUNC (svr_opt_cancel_clicked),
                      NULL);

  btn_help = gtk_button_new_with_label (_("Help"));
  gtk_object_set_data (GTK_OBJECT (dlg_server_options), "btn_help", btn_help);
  gtk_widget_show (btn_help);
  gtk_container_add (GTK_CONTAINER (hbuttonbox9), btn_help);
  gtk_signal_connect (GTK_OBJECT (btn_help), "clicked",
                      GTK_SIGNAL_FUNC (svr_opt_help_clicked),
                      NULL);

  return dlg_server_options;
}

