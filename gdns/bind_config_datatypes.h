/*
 * This file contains simple data types for
 * the bind 8 configuration files
 */

#ifndef BIND_CONFIG_DATATYPES_H
#define BIND_CONFIG_DATATYPES_H

#include "bind_common_datatypes.h"

/* this is for the key in the config file */

typedef struct bind_key_tag
{
  char *algorithm_id;
  char *secret_string;
} bind_key_t;

/* This is a number type.  It's supposed to be just a positive number */

typedef unsigned int bind_number_t;

/* This is a bind version.  It can be unlimited or a number */

typedef struct bind_version_tag
{
  bind_bool_t                           unlimited;
  bind_number_t                         version;
} bind_version_t;

/* This is a syslog type. ( kern | user | mail ... ) */

typedef char * bind_syslog_t;

/* This is the severity.  It can be a name ( like the syslog type )
   and has an optional severity level */

typedef struct bind_severity_tag
{
  char                                 *severity;
  int                                   debug_level;
} bind_severity_t;

/* These are all of the server options available */

typedef struct bind_options_tag
{
  char                                  *directory;
  char                                  *named_xfer;
  char                                  *dump_file;
  char                                  *memstatistics_file;
  char                                  *pid_file;
  char                                  *statistics_file;
  bind_bool_t                            auth_nxdomain;
  bind_bool_t                            deallocate_on_exit;
  bind_bool_t                            fake_iquery;
  bind_bool_t                            fetch_glue;
  bind_bool_t                            multiple_cnames;
  bind_bool_t                            notify;
  bind_bool_t                            recursion;
  bind_forward_t                         forward;
  bind_addr_match_list_t                *forwarders;
  bind_name_check_t                      check_names_master;
  bind_name_check_t                      check_names_slave;
  bind_name_check_t                      check_names_response;
  bind_addr_match_list_t                *allow_query;
  bind_addr_match_list_t                *allow_transfer;
  bind_addr_match_port_pair_t           *listen_on;
  bind_addr_t                           *query_source_addr;
  char                                   query_source_port;
  unsigned int                           max_transfer_time_in;
  bind_transfer_type_t                   transfer_format;
  unsigned int                           transfers_in;
  unsigned int                           transfers_out;
  unsigned int                           transfers_per_ns;
  bind_size_spec_t                       coresize;
  bind_size_spec_t                       datasize;
  bind_size_spec_t                       files;
  bind_size_spec_t                       stacksize;
  bind_number_t                          cleaning_interval;
  bind_number_t                          interface_interval;
  bind_number_t                          statistics_interval;
  bind_addr_match_list_t                *topology;
} bind_options_t;

/* This is a channel description in the config file */

typedef struct bind_channel_tag
{
  char                                 *channel_name;
  char                                 *file;
  bind_version_t                        versions;
  bind_size_spec_t                      size;
  bind_syslog_t                         syslog;
  bind_severity_t                      *severity;
  bind_bool_t                           print_category;
  bind_bool_t                           print_severity;
  bind_bool_t                           print_time;
} bind_channel_t;

/* These are the categories */

typedef struct bind_category_tag
{
  char                                  *category_name;
  bind_channel_t                        *channel_names;
} bind_category_t;

/* This is the combination of the channels and the categories,
   the logging statement in the config file */

typedef struct bind_logging_tag
{
  bind_channel_t                        *channels;
  bind_category_t                       *categories;
} bind_logging_t;

/* These are the types of zones that you can have defined */

typedef enum bind_zone_type_tag
{
  bind_zone_type_hint = 1,
  bind_zone_type_slave,
  bind_zone_type_stub,
  bind_zone_type_master,
} bind_zone_type_t;

/* These are the types of classes that you can have */

typedef enum bind_class_tag
{
  bind_class_in = 1,
  bind_class_hs,
  bind_class_hesiod,
  bind_class_chaos,
} bind_class_t;
    
/* This is a zone description */

typedef struct bind_zone_tag
{
  char                                  *domain_name;
  bind_class_t                           class;
  bind_zone_type_t                       type;
  char                                  *file;
  bind_addr_t                           *masters;
  bind_name_check_t                      check_names;
  bind_addr_match_list_t                *allow_update;
  bind_addr_match_list_t                *allow_transfer;
  bind_bool_t                            notify;
  bind_addr_t                           *also_notify;
} bind_zone_t;

/* This is for servers */

typedef struct bind_server_tag
{
  bind_addr_t                            ip_addr;
  bind_bool_t                            bogus;
  bind_number_t                          transfers;
  bind_transfer_type_t                   transfer_format;
  bind_key_t                            *keys;
} bind_server_t;

#endif /* BIND_CONFIG_DATATYPES_H */
