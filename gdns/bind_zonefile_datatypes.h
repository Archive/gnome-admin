/*
 * This header file contains information describing
 * the resource records found in dns zone files
 */

#ifndef BIND_ZONEFILE_DATATYPES_H
#define BIND_ZONEFILE_DATATYPES_H

#include "bind_common_datatypes.h"

typedef struct bind_record_ns_tag
{
  bind_class_t                      class;
  char                             *record;
} bind_record_ns_t;

typedef struct bind_record_a_tag
{
  bind_class_t                      class;
  char                             *record;
} bind_record_a_t;

typedef struct bind_record_mx_tag
{
  bind_class_t                      class;
  int                               value;
  char                             *record;
} bind_record_mx_t;

typedef struct bind_record_ptr_tag
{
  int                               value;
  bind_class_t                      class;
  char                             *record;
} bind_record_ptr_t;


typedef struct bind_record_soa_tag
{
  bind_class_t                      class;
  char                             *soa;
  char                             *contact;
  bind_number_t                     serial;
  bind_number_t                     refresh;
  bind_number_t                     retry;
  bind_number_t                     expire;
  bind_number_t                     default_ttl;
} bind_record_soa_t;

#endif /* BIND_ZONEFILE_DATATYPES_H */
