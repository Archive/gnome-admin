/* Printer control routines */
#include <objc/Object.h>

int doRunCmd(const char *path, ...);

@class Printer;
@interface PrintJob : Object
{
@protected
  int jobid;
  int jobsize;
  char *filename;
  char *user;
  Printer *printer;
}
- initWithJobInfo:(int) initjobid
	     Size:(int) initsize
	 Filename:(char *) initfilename
	     User:(char *) inituser
	 aPrinter:(Printer *) initprinter;
- (int)getJobSize;
- (int)getJobID;
- (const char *)getJobFilenames;
- (const char *)getJobOwner;
- (Printer *)getPrinter;
- (int)cancel; /* returns 1 on success, 0 on failure */
@end

@interface Printer : Object
{
@public
  PrintJob **jobs;
  int numjobs;
@protected
  char *queuename;
  char *lpc_path;
  int njoballoc;
}
- initWithName:(char *) initqueuename;
- getJobs;
- (const char *) getQueueName;
- (BOOL)isUp;
- (BOOL)isClean;
- (BOOL)isStarted;
/* These all return 1 for success, 0 for not */
- (int)up;
- (int)down;
- (int)clean;
- (int)restart;
- (int)start;
- (int)stop;
@end

/* Reads in /etc/printcap and creates Printer objects for all the printers
   listed */
@interface PrintSystem : Object
{
@public
  Printer **printers;
  int numprinters;
@protected
  int nprtalloc;
}
- (BOOL)exists;
@end
