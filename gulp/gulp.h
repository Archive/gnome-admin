#include <obgnome/obgnome.h>

@interface GULPMainWin : Gnome_AppWin
{
@public
   id app;
}
- menu_help_about:(id) anobj;
- menu_file_exit:(id) anobj;
@end

@interface GULPApp : Gnome_App
{
  PrintSystem *printsys;
  GULPMainWin *mainwin;

  Gtk_CList *printer_list;
  Gtk_CList *job_list;

  GdkPixmap *yes_pixmap, *no_pixmap;
  GdkBitmap *yes_mask, *no_mask;

  /* Buttons */

  Gtk_CheckButton *queueing;
  Gtk_CheckButton *printing;
  id clear_queue;
  id restart;
  id cancel_job;
	
  BOOL dotogglehandler;
  BOOL doselecthandler;

  GdkCursor *wait_cursor;
}
- do_job_list;
- do_printer_list;
- set_wait_cursor:(gint) wait;
@end

@interface Gtk_MessageBox : Gtk_Dialog
{
	Gtk_Button *btn_ok;
	Gtk_Label *lbl_msg;
}
- initWithLabel:(gchar *) label;
- run;
- clicked:(id) obj;
- destroy:(id) obj;
@end
