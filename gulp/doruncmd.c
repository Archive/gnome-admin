#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdarg.h>

int doRunCmd(const char *path, ...)
{
	va_list ap;
	int childpid, i, retstatus;
	int items = 0;
	char **args;

	va_start (ap, path);
	while (va_arg (ap, char *) != 0)
		items++;
	va_end (ap);

	va_start (ap, path);
	args = malloc ((items + 1) * sizeof (char *));
	for (i = 0; i < items; i++)
		args [i] = va_arg (ap, char *);
	args [i] = NULL;
		
	if ((childpid = fork())){
		free (args);
		if (waitpid (childpid, &retstatus, 0) <= 0)
			return -1;
		else
			return retstatus;
	}
	for(i = 0; i < 256; i++)
		close(i);

	execv (path, args);
	
	/* If we get here, we have failed miserably */
	exit(1);
}
