# Galician translation of gnome-admin
# Copyright (C) 1999 Free Software Foundation, Inc.
# Ruben Lopez Gomez <ryu@mundivia.es>, 1999.
#

msgid ""
msgstr ""
"Project-Id-Version: gnome-admin VERSION\n"
"POT-Creation-Date: 1999-02-14 16:47-0500\n"
"PO-Revision-Date: 1999-09-22 18:02+0100\n"
"Last-Translator: Ruben Lopez Gomez <ryu@mundivia.es>\n"
"Language-Team: Galician <gpul-traduccion@ceu.fi.udc.es>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#: gdns/prototype/gdns.c:114
msgid "gdns - Zone List"
msgstr "gdns - Lista de Zonas"

#: gdns/prototype/gdns.c:127 gdns/prototype/gdns.c:388
#: gdns/prototype/gdns.c:676 gdns/prototype/gdns.c:883 logview/logview.c:135
msgid "File"
msgstr "Ficheiro"

#: gdns/prototype/gdns.c:136
msgid "New"
msgstr "Novo"

#: gdns/prototype/gdns.c:148
msgid "Open..."
msgstr "Abrir..."

#: gdns/prototype/gdns.c:163 gdns/prototype/gdns.c:397
msgid "Save"
msgstr "Gardar"

#: gdns/prototype/gdns.c:173
msgid "Save As..."
msgstr "Gardar Como..."

#: gdns/prototype/gdns.c:186
msgid "Exit"
msgstr "Sair"

#: gdns/prototype/gdns.c:196 gdns/prototype/gdns.c:424
msgid "Edit"
msgstr "Editar"

#: gdns/prototype/gdns.c:205 gdns/prototype/gdns.c:433
msgid "Cut"
msgstr "Cortar"

#: gdns/prototype/gdns.c:215 gdns/prototype/gdns.c:443
msgid "Copy"
msgstr "Copiar"

#: gdns/prototype/gdns.c:225 gdns/prototype/gdns.c:453
msgid "Paste"
msgstr "Pegar"

#: gdns/prototype/gdns.c:240 gdns/prototype/gdns.c:468
#: gdns/prototype/gdns.c:864 gdns/prototype/gdns.c:1831
#: gdns/prototype/gdns.c:1947 gdns/prototype/gdns.c:1963
msgid "Delete"
msgstr "Borrar"

#: gdns/prototype/gdns.c:255
msgid "Server Options..."
msgstr "Opci�ns do servidor..."

#: gdns/prototype/gdns.c:265 gdns/prototype/gdns.c:321
msgid "Zone"
msgstr "Zona"

#: gdns/prototype/gdns.c:274 gdns/prototype/gdns.c:487
msgid "New..."
msgstr "Novo..."

#: gdns/prototype/gdns.c:284 gdns/prototype/gdns.c:497 logview/actions.c:120
msgid "Edit..."
msgstr "Editar..."

#: gdns/prototype/gdns.c:294 gdns/prototype/gdns.c:623
#: gdns/prototype/gdns.c:757 gdns/prototype/gdns.c:1042
#: gdns/prototype/gdns.c:1291 gdns/prototype/gdns.c:2083 logview/logview.c:141
msgid "Help"
msgstr "Axuda"

#: gdns/prototype/gdns.c:303
msgid "About..."
msgstr "Acerca de..."

#: gdns/prototype/gdns.c:326 gdns/prototype/gdns.c:522
msgid "Type"
msgstr "Tipo"

#: gdns/prototype/gdns.c:331
msgid "Servers / Source filename"
msgstr "Servidores / Ficheiros fontes"

#: gdns/prototype/gdns.c:375
msgid "Zone Editor"
msgstr "Editor de Zonas"

#: gdns/prototype/gdns.c:414 gulp/gulp.m:27
msgid "Close"
msgstr "Pechar"

#: gdns/prototype/gdns.c:478
msgid "Record"
msgstr "Grabar"

#: gdns/prototype/gdns.c:517
msgid "Name"
msgstr "Nome"

#: gdns/prototype/gdns.c:527
msgid "Value"
msgstr "Valor"

#: gdns/prototype/gdns.c:557
msgid "Zone Type"
msgstr "Tipo de Zona"

#: gdns/prototype/gdns.c:570 gdns/prototype/gdns.c:1844
msgid "Master"
msgstr "Mestre"

#: gdns/prototype/gdns.c:581 gdns/prototype/gdns.c:1873
msgid "Slave"
msgstr "Escravo"

#: gdns/prototype/gdns.c:589
msgid "Stub"
msgstr "Resto"

#: gdns/prototype/gdns.c:597
msgid "Hint"
msgstr "Indicador"

#: gdns/prototype/gdns.c:615 gdns/prototype/gdns.c:749
#: gdns/prototype/gdns.c:1034 gdns/prototype/gdns.c:1283
#: gdns/prototype/gdns.c:2075 logview/actions.c:141 logview/actions.c:662
msgid "Cancel"
msgstr "Cancelar"

#: gdns/prototype/gdns.c:659
msgid "New/Edit Hint Zone"
msgstr "Novo/Editar Indicador de Zona"

#: gdns/prototype/gdns.c:692
msgid ".."
msgstr ".."

#: gdns/prototype/gdns.c:700 gdns/prototype/gdns.c:985
#: gdns/prototype/gdns.c:1234
msgid "Check Names"
msgstr "Comprobar Nomes"

#: gdns/prototype/gdns.c:711 gdns/prototype/gdns.c:996
#: gdns/prototype/gdns.c:1245 gdns/prototype/gdns.c:1855
#: gdns/prototype/gdns.c:1884 gdns/prototype/gdns.c:1913
msgid "Warn"
msgstr "Aviso"

#: gdns/prototype/gdns.c:717 gdns/prototype/gdns.c:1002
#: gdns/prototype/gdns.c:1251 gdns/prototype/gdns.c:1861
#: gdns/prototype/gdns.c:1890 gdns/prototype/gdns.c:1919
msgid "Fail"
msgstr "Faio"

#: gdns/prototype/gdns.c:723 gdns/prototype/gdns.c:1008
#: gdns/prototype/gdns.c:1257 gdns/prototype/gdns.c:1867
#: gdns/prototype/gdns.c:1896 gdns/prototype/gdns.c:1925
msgid "Ignore"
msgstr "Ignorar"

#: gdns/prototype/gdns.c:739 gdns/prototype/gdns.c:1024
#: gdns/prototype/gdns.c:1273 gdns/prototype/gdns.c:2065 logview/misc.c:79
msgid "Ok"
msgstr "Aceptar"

#: gdns/prototype/gdns.c:815
msgid "New/Edit Slave/Stub Zone"
msgstr "Novo/Editar Escravo/Resto de Zona"

#: gdns/prototype/gdns.c:827
msgid "Name Servers"
msgstr "Servidores de Nomes"

#: gdns/prototype/gdns.c:846
msgid "label1"
msgstr "etiqueta1"

#: gdns/prototype/gdns.c:856 gdns/prototype/gdns.c:1823 logview/actions.c:113
msgid "Add..."
msgstr "Engadir..."

#: gdns/prototype/gdns.c:872
msgid "Zone Backups"
msgstr "Copias de seguridad de Zona"

#: gdns/prototype/gdns.c:893 gdns/prototype/gdns.c:1142
msgid "Access Control"
msgstr "Control de Acceso"

#: gdns/prototype/gdns.c:904 gdns/prototype/gdns.c:1153
msgid "Update..."
msgstr "Actualizar..."

#: gdns/prototype/gdns.c:912 gdns/prototype/gdns.c:1161
msgid "Query..."
msgstr "Pregunta..."

#: gdns/prototype/gdns.c:920 gdns/prototype/gdns.c:1169
msgid "Transfer..."
msgstr "Transferir..."

#: gdns/prototype/gdns.c:928 gdns/prototype/gdns.c:1177
msgid "Zone Transfers"
msgstr "Transferencia de Zonas"

#: gdns/prototype/gdns.c:939 gdns/prototype/gdns.c:1188
msgid "Longest Transfer"
msgstr "Transferencia Maior"

#: gdns/prototype/gdns.c:949 gdns/prototype/gdns.c:1198
msgid "(seconds)"
msgstr "(segundos)"

#: gdns/prototype/gdns.c:954 gdns/prototype/gdns.c:1203
msgid "DNS NOTIFY"
msgstr "Notificaci�n DNS"

#: gdns/prototype/gdns.c:965 gdns/prototype/gdns.c:1214
msgid "On"
msgstr "Aceso"

#: gdns/prototype/gdns.c:971 gdns/prototype/gdns.c:1220
msgid "Off"
msgstr "Apagado"

#: gdns/prototype/gdns.c:977 gdns/prototype/gdns.c:1226
msgid "Also Notify..."
msgstr "Tamen Notificar..."

#: gdns/prototype/gdns.c:1095
msgid "New/Edit Master Zone"
msgstr "Nova/Editar Zona Mestre"

#: gdns/prototype/gdns.c:1107
msgid "Zone Records"
msgstr "Rexistros de Zona"

#: gdns/prototype/gdns.c:1113
msgid "Edit Zone Resource Records..."
msgstr "Editar Recursos de Rexistro de Zona..."

#: gdns/prototype/gdns.c:1121
msgid "On-disk Storage"
msgstr "Gardar en disco"

#: gdns/prototype/gdns.c:1132
msgid "Zone File"
msgstr "Ficheiro de Zona"

#: gdns/prototype/gdns.c:1436
msgid "Server Options"
msgstr "Opci�ns de Servidor"

#: gdns/prototype/gdns.c:1453
msgid "Master directory"
msgstr "Directorio Mestre"

#: gdns/prototype/gdns.c:1460
msgid "named.xfer program"
msgstr "programa named.xfer"

#: gdns/prototype/gdns.c:1467
msgid "Dump file"
msgstr "Ficheiro de volcado"

#: gdns/prototype/gdns.c:1474
msgid "Mem stats file"
msgstr "Ficheiro de estadisticas de memoria"

#: gdns/prototype/gdns.c:1481
msgid "PID file"
msgstr "ficheiro de PID"

#: gdns/prototype/gdns.c:1488
msgid "Statistics file"
msgstr "Ficheiro de estadisticas"

#: gdns/prototype/gdns.c:1536
msgid "Auth NX Domain"
msgstr "Dominio de autentificaci�n NX"

#: gdns/prototype/gdns.c:1542
msgid "Dealloc on exit"
msgstr "Facer Dealloc � sair"

#: gdns/prototype/gdns.c:1548
msgid "Fake IQUERY"
msgstr "Sobrepasar IQUERY"

#: gdns/prototype/gdns.c:1554
msgid "Fetch glue records"
msgstr "Recuperar rexistros unidos"

#: gdns/prototype/gdns.c:1560
msgid "Recursive searches"
msgstr "B�squedas recursivas"

#: gdns/prototype/gdns.c:1566
msgid "Send NOTIFY messages"
msgstr "Enviar mensaxes NOTIFY"

#: gdns/prototype/gdns.c:1572
msgid "Support multiple CNAMEs"
msgstr "Soportar CNAMEs m�ltiples"

#: gdns/prototype/gdns.c:1578
msgid "Keep per-host statistics"
msgstr "Gardar estatisticas por h�spede"

#: gdns/prototype/gdns.c:1589
msgid "Core size"
msgstr "Tama�o de Core"

#: gdns/prototype/gdns.c:1595
msgid "Data size"
msgstr "Tama�o de datos"

#: gdns/prototype/gdns.c:1601
msgid "Stack size"
msgstr "Tama�o de pila"

#: gdns/prototype/gdns.c:1607
msgid "Open files"
msgstr "Ficheiros Abertos"

#: gdns/prototype/gdns.c:1619 gdns/prototype/gdns.c:1642
#: gdns/prototype/gdns.c:1665 gdns/prototype/gdns.c:1688
msgid "default"
msgstr "por defecto"

#: gdns/prototype/gdns.c:1625 gdns/prototype/gdns.c:1648
#: gdns/prototype/gdns.c:1671 gdns/prototype/gdns.c:1694
msgid "unlimited"
msgstr "ilimitado"

#: gdns/prototype/gdns.c:1710
msgid "Cleaning interval (minutes)"
msgstr "Intervalo de limpeza (minutos)"

#: gdns/prototype/gdns.c:1716
msgid "Interface scan interval (minutes)"
msgstr "Intervalo de proba de interfaz (minutos)"

#: gdns/prototype/gdns.c:1722
msgid "Statistics interval (minutes)"
msgstr "Intervalo de estad�sticas (minutos)"

#: gdns/prototype/gdns.c:1756
msgid "Forward"
msgstr "Remitir"

#: gdns/prototype/gdns.c:1761
msgid "first"
msgstr "primeiro"

#: gdns/prototype/gdns.c:1767
msgid "only"
msgstr "so"

#: gdns/prototype/gdns.c:1783
msgid "Forward-to IP addresses:"
msgstr "Remitir a unha direcci�n IP"

#: gdns/prototype/gdns.c:1902
msgid "Response"
msgstr "Responder"

#: gdns/prototype/gdns.c:1942 gdns/prototype/gdns.c:1958
msgid "Add"
msgstr "Engadir"

#: gdns/prototype/gdns.c:1979
msgid "Allow queries"
msgstr "Permitir preguntas"

#: gdns/prototype/gdns.c:2005
msgid "Allow zone xfers"
msgstr "Permitir transferencias de zona"

#: gdns/prototype/gdns.c:2020
msgid "Paths"
msgstr "Cami�os"

#: gdns/prototype/gdns.c:2025
msgid "Options"
msgstr "Opci�ns"

#: gdns/prototype/gdns.c:2030
msgid "Limits"
msgstr "L�mites"

#: gdns/prototype/gdns.c:2035
msgid "Periodic"
msgstr "Peri�dico"

#: gdns/prototype/gdns.c:2040
msgid "Forwarding"
msgstr "Reenv�o"

#: gdns/prototype/gdns.c:2045
msgid "Name Ck"
msgstr "Control de nome"

#: gdns/prototype/gdns.c:2050
msgid "Access Ctl"
msgstr "Control de acceso"

#: gulp/gulp.m:36
msgid "Elliot Lee (sopwith@cuc.edu)"
msgstr "Elliot Lee (sopwith@cuc.edu)"

#: gulp/gulp.m:37
msgid "Federico Mena (quartic@gimp.org)"
msgstr "Federico Mena (quartic@gimp.org)"

#: gulp/gulp.m:41
msgid "GULP"
msgstr "GULP"

#. copyright notice
#: gulp/gulp.m:43
msgid "(C) 1998 the Free Software Foundation"
msgstr "(C) 1998 the Free Software Foundation"

#. another comments
#: gulp/gulp.m:46
msgid "GNOME Unified Link to Printers program."
msgstr "Enlace unificado de GNOME a programas de impresi�n."

#: gulp/gulp.m:89 gulp/gulp.m:141
msgid "You must select a printer"
msgstr "Tes que elixir unha impresora"

#: gulp/gulp.m:119 gulp/gulp.m:177
msgid "Operation failed"
msgstr "Faio de operaci�n"

#: gulp/gulp.m:159
msgid "You must select a job"
msgstr "Tes que elixir un traballo"

#: gulp/gulp.m:174
msgid "Click on unknown widget\n"
msgstr "Click nun widget desco�ecido\n"

#: gulp/gulp.m:207
msgid "Job ID"
msgstr "ID de traballo"

#: gulp/gulp.m:208
msgid "Filenames"
msgstr "Nomes de ficheiro"

#: gulp/gulp.m:209
msgid "Size"
msgstr "Tama�o"

#: gulp/gulp.m:210
msgid "Owner"
msgstr "Propietario"

#: gulp/gulp.m:285
msgid "Printer"
msgstr "Impresora"

#: gulp/gulp.m:286
msgid "Jobs"
msgstr "Traballo"

#: gulp/gulp.m:287 gulp/gulp.m:468
msgid "Queueing"
msgstr "Cola"

#: gulp/gulp.m:288 gulp/gulp.m:469
msgid "Printing"
msgstr "Impresi�n"

#: gulp/gulp.m:308
#, c-format
msgid "oldoffset for printer_list is %d\n"
msgstr "O antigo desprazamento para a lista de impresi�n � %d\n"

#: gulp/gulp.m:331
#, c-format
msgid "Doing row %d\n"
msgstr "Facenco a fila %d\n"

#: gulp/gulp.m:347
#, c-format
msgid "Setting row data %d to %#lx\n"
msgstr "Po�endo datos da file %d a %#lx\n"

#: gulp/gulp.m:383
msgid "E_xit"
msgstr "_Sair"

#: gulp/gulp.m:391
msgid "_About..."
msgstr "_Acerca de"

#: gulp/gulp.m:397
msgid "_Program"
msgstr "_Programa"

#: gulp/gulp.m:399
msgid "_Help"
msgstr "A_xuda"

#: gulp/gulp.m:414
msgid "Printing system not installed/configured."
msgstr "O sistema de impresi�n non est� instalado/configurado."

#: gulp/gulp.m:423
msgid "GNOME Unified Link to Printers"
msgstr "Enlace unificado de GNOME para impresoras"

#: gulp/gulp.m:449
msgid "Printer queues"
msgstr "Colas de impresi�n"

#: gulp/gulp.m:470
msgid "Clear queue"
msgstr "Limpar cola"

#: gulp/gulp.m:471
msgid "Restart daemon"
msgstr "Recomezar demonio"

#. Jobs
#: gulp/gulp.m:485
msgid "Jobs for selected printer"
msgstr "Traballos para a impresora elixida"

#: gulp/gulp.m:498
msgid "Cancel job"
msgstr "Cancelar traballo"

#: logview/logview.c:138
#, fuzzy
msgid "F_ilter"
msgstr "Filtrar"

#: logview/about.c:61
msgid "This  program  is  part of  the  GNOME  project for  Linux."
msgstr "Estr programa � parte do proxecto GNOME para Linux."

#: logview/about.c:62
msgid "Logview comes with ABSOLUTELY NO WARRANTY. This"
msgstr "Logview ven sen ABSOLUTAMENTE NINGUNHA GARANT�A. Este"

#: logview/about.c:63
msgid "is free software, and you are welcome to redistribute it"
msgstr "� software libre, e ti es benvido a redistribuilo"

#: logview/about.c:64
msgid "under the conditions of the GNU General Public Licence. The "
msgstr "baixo as condici�ns da licencia publica xeral de GNU (GPL). O"

#: logview/about.c:65
msgid "log icon is a courtesy of Tuomas Kuosmanen (a.k.a tigert)."
msgstr "icono de log e cortes�a de Tuomas Kuosmanen (a.k.a tigert)."

#: logview/about.c:94
#, c-format
msgid "Version %s"
msgstr "Versi�n %s"

#: logview/about.c:97
msgid "Written by:"
msgstr "Escrito por:"

#: logview/about.c:99
msgid "Cesar Miquel"
msgstr "Cesar Miquel"

#: logview/about.c:101
msgid "email:"
msgstr "correo electr�nico:"

#: logview/about.c:103
msgid "email: "
msgstr "correo electr�nico:"

#: logview/about.c:142
msgid "Cesar Miquel (miquel@df.uba.ar)"
msgstr "Cesar Miquel (miquel@df.uba.ar)"

#: logview/about.c:143
msgid ""
"This  program  is  part of  the  GNOME  project for Linux. Logview comes "
"with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to "
"redistribute it under the conditions of the GNU General Public Licence. The "
"log icon is a courtesy of Tuomas Kuosmanen (a.k.a tigert)."
msgstr ""
"Este programa � parte do proxecto GNOME para Linux. Logview ven sen "
"ABSOLUTAMENTE NINGUNHA GARANT�A. Este e software libre, e ti es benvido a "
"redistribuilo baixo as condici�ns da licencia publica xeral de GNU (GPL). O "
"icono de log � cortes�a de Tuomas Kuosmanen (a.k.a tigert)."

#. go get logview.xpm in $(prefix)/share/pixmaps/logview
#: logview/about.c:150
msgid "Logview"
msgstr "Logview"

#: logview/about.c:151
msgid "Copyright (C) 1998"
msgstr "Copyright (C) 1998"

#: logview/actions.c:56
msgid "Action database"
msgstr "Base de datos de acci�ns"

#: logview/actions.c:61 logview/monitor.c:114
msgid "Monitor options"
msgstr "Opci�ns do monitor"

#: logview/actions.c:127
msgid "Remove"
msgstr "Eliminar"

#: logview/actions.c:146 logview/actions.c:667
msgid "OK"
msgstr "Aceptar"

#: logview/actions.c:281 logview/actions.c:294 logview/actions.c:305
#: logview/actions.c:316 logview/actions.c:327 logview/actions.c:338
msgid "Error parsing actions data base"
msgstr "Error analizando a base de datos de acci�ns"

#: logview/actions.c:370
msgid "Can't write to actions database!"
msgstr "Non poido escribir acci�ns � base de datos!"

#: logview/actions.c:476
msgid "<empty>"
msgstr "<valeiro>"

#: logview/actions.c:477
msgid "log. name regexp"
msgstr "expresi�n regular do nome de log."

#: logview/actions.c:478
msgid "process regexp"
msgstr "expresi�n regular de proceso"

#: logview/actions.c:479
msgid "message regexp"
msgstr "expresi�n regular de mensaxe"

#: logview/actions.c:480
msgid "action to execute when regexps are TRUE"
msgstr "Acci�n a executar cando as expresi�ns regulares son CERTAS"

#: logview/actions.c:481
msgid "description"
msgstr "descripci�n"

#: logview/actions.c:638
msgid "description:"
msgstr "descripci�n:"

#: logview/actions.c:647
msgid "Description of this entry."
msgstr "Descripci�n deste cadro de texto."

#: logview/actions.c:709
#, c-format
msgid ""
"tag: [%s]\n"
"log_name: [%s]\n"
"process: [%s]\n"
"message: [%s]\n"
"description: [%s]\n"
"action: [%s]\n"
msgstr ""
"marca: [%s]\n"
"nome_do_log: [%s]\n"
"proceso: [%s]\n" 
"mensaxe: [%s]\n" 
"descrici�n: [%s]\n"
"acci�n: [%s]\n"


#: logview/calendar.c:132
msgid "Calendar"
msgstr "Calendario"

#: logview/calendar.c:152
msgid "Prev Month"
msgstr "Mes anterior"

#: logview/calendar.c:159
msgid "Next Month"
msgstr "Mes seguinte"

#: logview/calendar.c:167
msgid "Hide calendar"
msgstr "Ocultar calendario"

#: logview/calendar.c:258
msgid "Program error: mark is NULL!"
msgstr "Erro programa: a marca e NULL!"

#: logview/calendar.c:275
msgid "SUN"
msgstr "DOM"

#: logview/calendar.c:275
msgid "MON"
msgstr "LUN"

#: logview/calendar.c:275
msgid "TUE"
msgstr "MAR"

#: logview/calendar.c:275
msgid "WED"
msgstr "MER"

#: logview/calendar.c:276
msgid "THU"
msgstr "XOV"

#: logview/calendar.c:276
msgid "FRI"
msgstr "VEN"

#: logview/calendar.c:276
msgid "SAT"
msgstr "SAB"

#: logview/info.c:80
msgid "Log stats"
msgstr "estadisticas do rexistro"

#: logview/info.c:140
msgid "Log information"
msgstr "Informaci�n do rexistro"

#: logview/info.c:157
msgid "Log:"
msgstr "Rexistro:"

#: logview/info.c:159
msgid "Size:"
msgstr "Tama�o:"

#: logview/info.c:161
msgid "Modified:"
msgstr "Modificado:"

#: logview/info.c:163
msgid "Start date:"
msgstr "Data de comezo:"

#: logview/info.c:165
msgid "Last date:"
msgstr "�ltima data:"

#: logview/info.c:167
msgid "Num. lines:"
msgstr "Num. de l�neas:"

#: logview/info.c:183
#, c-format
msgid "%ld bytes"
msgstr "%ld bytes"

#: logview/log_repaint.c:86
msgid "January"
msgstr "Xaneiro"

#: logview/log_repaint.c:86
msgid "February"
msgstr "Febreiro"

#: logview/log_repaint.c:86
msgid "March"
msgstr "Marzo"

#: logview/log_repaint.c:86
msgid "April"
msgstr "Abril"

#: logview/log_repaint.c:86
msgid "May"
msgstr "Maio"

#: logview/log_repaint.c:87
msgid "June"
msgstr "Xu�o"

#: logview/log_repaint.c:87
msgid "July"
msgstr "Xullo"

#: logview/log_repaint.c:87
msgid "August"
msgstr "Agosto"

#: logview/log_repaint.c:87
msgid "September"
msgstr "Septembro"

#: logview/log_repaint.c:87
msgid "October"
msgstr "Octubro"

#: logview/log_repaint.c:88
msgid "November"
msgstr "Novembre"

#: logview/log_repaint.c:88
msgid "December"
msgstr "Decembro"

#: logview/logrtns.c:96
msgid "Not enough memory!\n"
msgstr "Memoria insuficiente!\n"

#: logview/logrtns.c:103
msgid "Out of memory!\n"
msgstr "Sen memoria!\n"

#: logview/logrtns.c:128
msgid "Unable to open logfile!\n"
msgstr "� incposible abrir o ficheiro!\n"

#: logview/logrtns.c:182
#, c-format
msgid "%s is not a regular file."
msgstr "%s non e un ficheiro regular."

#: logview/logrtns.c:189
#, c-format
msgid ""
"%s is not user readable. Probably has read-only permission. Either run the "
"program as root or ask the sysadmin to change the permissions on the file."
msgstr ""
"%s non e lexible polo usuario. Probablemente ten permisos de s� lectura. "
"Exec�tao coma root, ou ben p�delle � administrador que cambie os permisos "
"do ficheiro."

#: logview/logrtns.c:201
#, c-format
msgid "%s could not be opened."
msgstr "%s non se pode abrir."

#: logview/logrtns.c:210 logview/logrtns.c:220
#, c-format
msgid "%s not a log file."
msgstr "%s non e un ficheiro de log."

#: logview/logrtns.c:618 logview/logrtns.c:649 logview/logrtns.c:780
msgid "ReadLogStats: out of memory"
msgstr "ReadLogStats: sen memoria"

#: logview/logview.c:82
msgid "Open log...            "
msgstr "Abrir log...           "

#: logview/logview.c:83
msgid "Open log"
msgstr "Abrir log"

#: logview/logview.c:85
msgid "Export log...          "
msgstr "Exportar log...        "

#: logview/logview.c:86
msgid "Export log"
msgstr "Exportar log"

#: logview/logview.c:88
msgid "Close log              "
msgstr "Pechar log             "

#: logview/logview.c:89
msgid "Close log"
msgstr "Pechar log"

#: logview/logview.c:91
msgid "Switch log             "
msgstr "Cambiar a log          "

#: logview/logview.c:92
msgid "Switch log"
msgstr "Cambiar a log"

#: logview/logview.c:94
msgid "Monitor..              "
msgstr "Monitorizar log..      "

#: logview/logview.c:95
msgid "Monitor log"
msgstr "Monitorizar log"

#: logview/logview.c:97
msgid "Exit                   "
msgstr "Sair                   "

#: logview/logview.c:98
msgid "Exit program"
msgstr "Sair do programa"

#: logview/logview.c:104
msgid "Calendar                "
msgstr "Calendario              "

#: logview/logview.c:105
msgid "Show calendar log"
msgstr "Ver log do calendario"

#: logview/logview.c:107
msgid "Log stats               "
msgstr "Estadisticas de rexistro"

#: logview/logview.c:108
msgid "Show log stats"
msgstr "Ver estadisticas de rexistro"

#: logview/logview.c:110
msgid "Zoom                    "
msgstr "Ampliar                 "

#: logview/logview.c:111
msgid "Show line info"
msgstr "Ver informaci�n de li�a"

#: logview/logview.c:117
msgid "Select...               "
msgstr "Elexir...               "

#: logview/logview.c:118
msgid "Select log events"
msgstr "Elexir eventos do log"

#: logview/logview.c:120
msgid "Filter..                "
msgstr "Filtrar..               "

#: logview/logview.c:121
msgid "Filter log events"
msgstr "Filtrar eventos do log"

#: logview/logview.c:127
msgid "About..                "
msgstr "Acerca de..            "

#: logview/logview.c:128
msgid "Info about logview"
msgstr "Informaci�n de logview"

#: logview/logview.c:137
msgid "View"
msgstr "Ver"

#: logview/logview.c:139
msgid "Filter"
msgstr "Filtrar"

#: logview/logview.c:229
msgid "No log files to open"
msgstr "Non hai ficheiros de log para abrir"

#. Create App
#: logview/logview.c:258
msgid "System Log Viewer"
msgstr "Visor do rexistro do sistema"

#: logview/logview.c:338
msgid "Filename: "
msgstr "Ficheiro:"

#: logview/logview.c:354
msgid "Date: "
msgstr "Data: "

#: logview/logview.c:569
msgid "Too many open logs. Close one and try again"
msgstr "Demasiados logs abertos. Pecha un e volve a intentalo"

#: logview/logview.c:624
msgid "Open new logfile"
msgstr "Abir novo ficheiro de log"

#: logview/misc.c:78
#, c-format
msgid "Error: [%s]\n"
msgstr "Erro: [%s]\n"

#: logview/monitor.c:131
msgid "Choose logs to monitor"
msgstr "Elexir os logs a monitorizar"

#. Check boxes
#: logview/monitor.c:210
msgid "Hide app"
msgstr "Esconder aplicaci�n"

#: logview/monitor.c:218
msgid "Exec actions"
msgstr "Executar acci�ns"

#: logview/monitor.c:282 logview/monitor.c:329
msgid "tmp_list is NULL\n"
msgstr "tmp_list � NULL\n"

#: logview/monitor.c:456
msgid "Monitoring logs.."
msgstr "Monitorizando logs.."

#: logview/monitor.c:634
msgid "TOUCHED!!\n"
msgstr "MODIFICADO!!\n"

#: logview/zoom.c:77
msgid "Zoom view"
msgstr "Aumentar Zoom"

#: logview/zoom.c:151
#, c-format
msgid "Log line detail for %s"
msgstr "Detalle de li�a de rexistro %s"

#: logview/zoom.c:153
msgid "Log line detail for <No log loaded>"
msgstr "Detalle de rexistro de li�a para <Non hai log cargado>"

#: logview/zoom.c:176
msgid "Date:"
msgstr "Data:"

#: logview/zoom.c:178
msgid "Process:"
msgstr "Proceso:"

#: logview/zoom.c:180
msgid "Message:"
msgstr "Mensaxe:"

#: logview/zoom.c:182
msgid "Description:"
msgstr "Descripci�n:"
