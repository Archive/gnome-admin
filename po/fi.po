# gnome-admin de.po file.
# Copyright (C) 1998 Free Software Foundation, Inc.
# Mikko Rauhala <mjr@iki.fi>, 1999
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-admin VERSION\n"
"POT-Creation-Date: 1999-09-11 03:16+0200\n"
"PO-Revision-Date: 1998-08-15 01:20+0100\n"
"Last-Translator: Mikko Rauhala <mjr@iki.fi>\n"
"Language-Team: Finnish <fi@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#: gulp/gulp.m:27
msgid "Close"
msgstr "Sulje"

#: gulp/gulp.m:36
msgid "Elliot Lee (sopwith@cuc.edu)"
msgstr "Elliot Lee (sopwith@cuc.edu)"

#: gulp/gulp.m:37
msgid "Federico Mena (quartic@gimp.org)"
msgstr "Federico Mena (qartic@gimp.org)"

#: gulp/gulp.m:41
msgid "GULP"
msgstr "GULP"

#. copyright notice
#: gulp/gulp.m:43
msgid "(C) 1998 the Free Software Foundation"
msgstr "(C) 1998 Free Software Foundation"

#. another comments
#: gulp/gulp.m:46
msgid "GNOME Unified Link to Printers program."
msgstr "GNOME:n tulostusjonojen hallinta"

#: gulp/gulp.m:89 gulp/gulp.m:140
msgid "You must select a printer"
msgstr "Sinun t�ytyy valita tulostin"

#: gulp/gulp.m:119 gulp/gulp.m:176
msgid "Operation failed"
msgstr "Toiminto ep�onnistui"

#: gulp/gulp.m:158
msgid "You must select a job"
msgstr "Sinun t�ytyy valita ty�"

#: gulp/gulp.m:173
msgid "Click on unknown widget\n"
msgstr "Paina tuntematonta elementti�\n"

#: gulp/gulp.m:213
msgid "Job ID"
msgstr "Ty�tunnus"

#: gulp/gulp.m:214
msgid "Filenames"
msgstr "Tiedostonimet"

#: gulp/gulp.m:215
msgid "Size"
msgstr "Koko"

#: gulp/gulp.m:216
msgid "Owner"
msgstr "Omistaja"

#: gulp/gulp.m:298
msgid "Printer"
msgstr "Tulostin"

#: gulp/gulp.m:299
msgid "Jobs"
msgstr "Ty�t"

#: gulp/gulp.m:300 gulp/gulp.m:480
msgid "Queueing"
msgstr "Jonotus"

#: gulp/gulp.m:301 gulp/gulp.m:481
msgid "Printing"
msgstr "Tulostus"

#: gulp/gulp.m:329
#, c-format
msgid "oldoffset for printer_list is %d\n"
msgstr "oldoffset printer_list:lle on %d\n"

#: gulp/gulp.m:349
#, c-format
msgid "Doing row %d\n"
msgstr "Teen rivi� %d\n"

#: gulp/gulp.m:365
#, c-format
msgid "Setting row data %d to %#lx\n"
msgstr "Asetan rivin %d tiedot %#lx:ksi\n"

#: gulp/gulp.m:426
msgid "Printing system not installed/configured."
msgstr "Tulostusj�rjestelm�� ei ole asennettu asianmukaisesti."

#: gulp/gulp.m:435
msgid "GNOME Unified Link to Printers"
msgstr "GNOME:n tulostusjonojen hallinta"

#: gulp/gulp.m:461
msgid "Printer queues"
msgstr "Tulostusjonot"

#: gulp/gulp.m:482
msgid "Clear queue"
msgstr "Tyhjenn� jono"

#: gulp/gulp.m:483
msgid "Restart daemon"
msgstr "K�ynnist� palvelin uudelleen"

#. Jobs
#: gulp/gulp.m:497
msgid "Jobs for selected printer"
msgstr "Valitun tulostimen ty�t"

#: gulp/gulp.m:510
msgid "Cancel job"
msgstr "Peruuta ty�"

#: logview/about.c:40
msgid "Cesar Miquel (miquel@df.uba.ar)"
msgstr "Cesar Miquel (miquel@df.uba.ar)"

#: logview/about.c:41
msgid ""
"This  program  is  part of  the  GNOME  project for Linux. Logview comes "
"with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to "
"redistribute it under the conditions of the GNU General Public Licence. The "
"log icon is a courtesy of Tuomas Kuosmanen (a.k.a tigert)."
msgstr ""
"T�m� ohjelma on osa GNOME-projektia. Lokilukijalle ei anneta MIT��N TAKUITA. "
"T�m� on vapaa ohjelmisto, ja olet tervetullut levitt�m��n sit� edelleen GNU "
"General Public License:n asettamin ehdoin. Log-ikoni on Tuomas Kuosmasen "
"(tigert) k�sialaa."

#. go get logview.xpm in $(prefix)/share/pixmaps/logview
#: logview/about.c:48
msgid "Logview"
msgstr "Lokilukija"

#: logview/about.c:49
msgid "Copyright (C) 1998"
msgstr "Copyright (C) 1998"

#: logview/actions.c:56
msgid "Action database"
msgstr "Toimintotietokanta"

#: logview/actions.c:61 logview/monitor.c:114
msgid "Monitor options"
msgstr "N�ytt�valinnat"

#: logview/actions.c:113
msgid "Add..."
msgstr "Lis��..."

#: logview/actions.c:120
msgid "Edit..."
msgstr "Muokkaa..."

#: logview/actions.c:127
msgid "Remove"
msgstr "Poista"

#: logview/actions.c:141 logview/actions.c:662
msgid "Cancel"
msgstr "Peruuta"

#: logview/actions.c:146 logview/actions.c:667
msgid "OK"
msgstr "OK"

#: logview/actions.c:281 logview/actions.c:294 logview/actions.c:305
#: logview/actions.c:316 logview/actions.c:327 logview/actions.c:338
msgid "Error parsing actions data base"
msgstr "Virhe lukiessa toimintotietokantaa"

#: logview/actions.c:370
msgid "Can't write to actions database!"
msgstr "Virhe kirjoittaessa toimintotietokantaa!"

#: logview/actions.c:476
msgid "<empty>"
msgstr "<tyhj�>"

#: logview/actions.c:477
msgid "log. name regexp"
msgstr "lokin nimen s��nn�llinen lauseke"

#: logview/actions.c:478
msgid "process regexp"
msgstr "processin s��nn�llinen lauseke"

#: logview/actions.c:479
msgid "message regexp"
msgstr "viestin s��nn�llinen lauseke"

#: logview/actions.c:480
msgid "action to execute when regexps are TRUE"
msgstr "suoritettava toiminto, kun lauseke tosi"

#: logview/actions.c:481
msgid "description"
msgstr "kuvaus"

#: logview/actions.c:638
msgid "description:"
msgstr "kuvaus:"

#: logview/actions.c:647
msgid "Description of this entry."
msgstr "T�m�n merkinn�n kuvaus."

#: logview/actions.c:709
#, c-format
msgid ""
"tag: [%s]\n"
"log_name: [%s]\n"
"process: [%s]\n"
"message: [%s]\n"
"description: [%s]\n"
"action: [%s]\n"
msgstr ""
"Merkki: [%s]\n"
"Lokin nimi: [%s]\n"
"Prosessi: [%s]\n"
"Viesti: [%s]\n"
"Kuvaus: [%s]\n"
"Toiminto: [%s]\n"

#: logview/calendar.c:118
msgid "Calendar"
msgstr "Kalenteri"

#: logview/calendar.c:139
msgid "Hide calendar"
msgstr "Piilota kalenteri"

#: logview/info.c:80
msgid "Log stats"
msgstr "Lokitilastot"

#: logview/info.c:140
msgid "Log information"
msgstr "Lokitiedot"

#: logview/info.c:157
msgid "Log:"
msgstr "Loki:"

#: logview/info.c:159
msgid "Size:"
msgstr "Koko:"

#: logview/info.c:161
msgid "Modified:"
msgstr "Muokattu:"

#: logview/info.c:163
msgid "Start date:"
msgstr "Alkup�iv�m��r�:"

#: logview/info.c:165
msgid "Last date:"
msgstr "Viimeisin p�iv�m��r�:"

#: logview/info.c:167
msgid "Num. lines:"
msgstr "Rivim��r�:"

#: logview/info.c:183
#, c-format
msgid "%ld bytes"
msgstr "%ld tavua"

#: logview/log_repaint.c:87
msgid "April"
msgstr "huhtikuu"

#: logview/log_repaint.c:87
msgid "February"
msgstr "helmikuu"

#: logview/log_repaint.c:87
msgid "January"
msgstr "tammikuu"

#: logview/log_repaint.c:87
msgid "March"
msgstr "maaliskuu"

#: logview/log_repaint.c:87
msgid "May"
msgstr "toukokuu"

#: logview/log_repaint.c:88
msgid "August"
msgstr "elokuu"

#: logview/log_repaint.c:88
msgid "July"
msgstr "hein�kuu"

#: logview/log_repaint.c:88
msgid "June"
msgstr "kes�kuu"

#: logview/log_repaint.c:88
msgid "October"
msgstr "lokakuu"

#: logview/log_repaint.c:88
msgid "September"
msgstr "syyskuu"

#: logview/log_repaint.c:89
msgid "December"
msgstr "joulukuu"

#: logview/log_repaint.c:89
msgid "November"
msgstr "marraskuu"

#: logview/logrtns.c:96
msgid "Not enough memory!\n"
msgstr "Ei tarpeeksi muistia!\n"

#: logview/logrtns.c:104
msgid "Out of memory!\n"
msgstr "Muisti loppu!\n"

#: logview/logrtns.c:129
msgid "Unable to open logfile!\n"
msgstr "Lokitiedoston avaus ep�onnistui!\n"

#: logview/logrtns.c:183
#, c-format
msgid "%s is not a regular file."
msgstr "%s ei ole tavallinen tiedosto."

#: logview/logrtns.c:190
#, c-format
msgid ""
"%s is not user readable. Probably has read-only permission. Either run the "
"program as root or ask the sysadmin to change the permissions on the file."
msgstr ""
"%s ei ole tavallisen k�ytt�j�n luettavissa. Aja ohjelma root-tunnuksella tai "
"pyyd� yll�pitoa vaihtamaan tiedoston oikeuksia."

#: logview/logrtns.c:202
#, c-format
msgid "%s could not be opened."
msgstr "Tiedostoa %s ei voitu avata."

#: logview/logrtns.c:211 logview/logrtns.c:221
#, c-format
msgid "%s not a log file."
msgstr "%s ei ole lokitiedosto."

#: logview/logrtns.c:614 logview/logrtns.c:645 logview/logrtns.c:776
msgid "ReadLogStats: out of memory"
msgstr "ReadLogStats: muisti loppu"

#: logview/logview.c:83
msgid "Open log...            "
msgstr "Avaa loki...           "

#: logview/logview.c:84
msgid "Open log"
msgstr "Avaa loki"

#: logview/logview.c:86
msgid "Export log...          "
msgstr "Tallenna loki...       "

#: logview/logview.c:87
msgid "Export log"
msgstr "Tallenna loki"

#: logview/logview.c:89
msgid "Close log              "
msgstr "Sulje loki             "

#: logview/logview.c:90
msgid "Close log"
msgstr "Log schlie�en"

#: logview/logview.c:92
msgid "Switch log             "
msgstr "Vaihda loki            "

#: logview/logview.c:93
msgid "Switch log"
msgstr "Vaihda loki"

#: logview/logview.c:95
msgid "Monitor..              "
msgstr "Valvo...               "

#: logview/logview.c:96
msgid "Monitor log"
msgstr "Valvo lokia"

#: logview/logview.c:98
msgid "Exit                   "
msgstr "Lopeta                 "

#: logview/logview.c:99
msgid "Exit program"
msgstr "Lopeta ohjelma"

#: logview/logview.c:105
msgid "Calendar                "
msgstr "Kalenteri               "

#: logview/logview.c:106
msgid "Show calendar log"
msgstr "N�yt� kalenteriloki"

#: logview/logview.c:108
msgid "Log stats               "
msgstr "Lokitilastot            "

#: logview/logview.c:109
msgid "Show log stats"
msgstr "N�yt� lokitilastot"

#: logview/logview.c:111
msgid "Zoom                    "
msgstr "Zoom                    "

#: logview/logview.c:112
msgid "Show line info"
msgstr "N�yt� rivitiedot"

#: logview/logview.c:118
msgid "Select...               "
msgstr "Valitse...              "

#: logview/logview.c:119
msgid "Select log events"
msgstr "Valitse lokitapahtumat"

#: logview/logview.c:121
msgid "Filter..                "
msgstr "Suodin...               "

#: logview/logview.c:122
msgid "Filter log events"
msgstr "Suodata lokitapahtumia"

#: logview/logview.c:128
msgid "About..                "
msgstr "Tietoja...             "

#: logview/logview.c:129
msgid "Info about logview"
msgstr "Tietoja lokilukijasta"

#: logview/logview.c:138
msgid "F_ilter"
msgstr "_Suodin"

#: logview/logview.c:227
msgid "No log files to open"
msgstr "Ei avattavia lokitiedostoja"

#. Create App
#: logview/logview.c:256
msgid "System Log Viewer"
msgstr "J�rjestelm�n lokien lukija"

#: logview/logview.c:357
msgid "Filename: "
msgstr "Tiedostonimi: "

#: logview/logview.c:373
msgid "Date: "
msgstr "P�iv�m��r�: "

#: logview/logview.c:620
msgid "Too many open logs. Close one and try again"
msgstr "Liikaa avoimia lokeja. Sulje yksi ja yrit� uudelleen"

#: logview/logview.c:673
msgid "Open new logfile"
msgstr "Avaa uusi lokitiedosto"

#: logview/misc.c:78
#, c-format
msgid "Error: [%s]\n"
msgstr "Virhe: [%s]\n"

#: logview/misc.c:79
msgid "Ok"
msgstr "Ok"

#: logview/monitor.c:131
msgid "Choose logs to monitor"
msgstr "Valitse valvottavat lokit"

#. Check boxes
#: logview/monitor.c:210
msgid "Hide app"
msgstr "Piilota sovellus"

#: logview/monitor.c:218
msgid "Exec actions"
msgstr "K�ynnistett�v�t toiminnot"

#: logview/monitor.c:282 logview/monitor.c:329
msgid "tmp_list is NULL\n"
msgstr "tmp_list on NULL\n"

#: logview/monitor.c:456
msgid "Monitoring logs.."
msgstr "Lokien valvonta..."

#: logview/monitor.c:634
msgid "TOUCHED!!\n"
msgstr "KOSKETTU!!\n"

#: logview/zoom.c:77
msgid "Zoom view"
msgstr "Zoomaa n�kym��"

#: logview/zoom.c:151
#, c-format
msgid "Log line detail for %s"
msgstr "Lokiriviyksityiskohdat %s:lle"

#: logview/zoom.c:153
msgid "Log line detail for <No log loaded>"
msgstr "Lokiriviyksityiskohdat <ei lokia ladattuna>:lle"

#: logview/zoom.c:176
msgid "Date:"
msgstr "P�iv�m��r�:"

#: logview/zoom.c:178
msgid "Process:"
msgstr "Prosessi:"

#: logview/zoom.c:180
msgid "Message:"
msgstr "Viesti:"

#: logview/zoom.c:182
msgid "Description:"
msgstr "Kuvaus:"
