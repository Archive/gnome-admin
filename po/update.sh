#!/bin/sh

xgettext --default-domain=gnome-admin --directory=.. \
  --add-comments --keyword=_ --keyword=N_ \
  --files-from=./POTFILES.in \
&& test ! -f gnome-admin.po \
   || ( rm -f ./gnome-admin.pot \
    && mv gnome-admin.po ./gnome-admin.pot )
